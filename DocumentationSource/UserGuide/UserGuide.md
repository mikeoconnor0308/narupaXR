# User Guide

This page is a guide for running the Narupa XR Front End.

@section settingup Setting up Narupa XR

This section servers as a guide for setting up the HTC Vive for use with Narupa in both single player and multi-user mode. 

## Equipment List 

* 1 x HTC Vive Headset
* 2 x HTC Vive Controllers
* 1 x HTC Vive Adapter Box
* 1 x HDMI Cable
* 1 x USB to USB Cable 
* 2 x Micro USB Cable
* 1 x HTC Vive Headset Power Adapter (short, thin cable)
* 2 x HTC Vive Lighthouses
* 2 x HTC Vive Lighthouses Power Adapter (long, thicker cable)
* 2 x 2m high camera tripods
* 2 x Camera Ball Mount
* 1-2 x Power Extension Cables 
* 1 4-way Extension cable

**Multiplayer**:

For multiplayer, all of the above is required for each VR player, except only one set of stands and light stations are required. Additionally, for optimum performance it should be set up to run on a local network:
* 1 LAN cable for each VR player
* One router

On each computer that you are running Narupa on, you will need the following: 

* An installation of [Steam](http://store.steampowered.com/about/).
* A steam [login](https://store.steampowered.com/join/?redir=about%2F) (you can use the same one on all computers).
* A copy of the Narupa XR front end. 

## Clearing a Space
Your first job will be to identify the space you will be using to set up the HTC Vive and Narupa. Here are some questions you should ask yourself -
* Is the space large enough? If you wish to use stand-up mode, you will need a minimum of 1.5m x 3m space. 
* Do you have adequate access to power?
* Is the space clear? When moving around the HTC Vive, you can’t see the real world around you - for health and safety, it is imperative that all tripping and knocking into hazards are removed. 
* Is the playspace obstructing any doors or exits? 

## Identifying Power Sockets

A typical setup will have many wires (until wireless VR comes to market). Where possible, sockets should be chosen to minimize the number of wires you have trailing around the room; not only does this look tidier, you are reducing the risk of someone tripping on a wire and either damaging the equipment, or worse, hurting themselves. 

Ideally, each sensor should be located directly next to a mains adapter, and the computer should be placed on a table which is directly next to a power socket. Doing so will minimize the trip hazard of your setup. 

Obviously, not every room will have these conditions, so if you do need to leave trailing wires, make sure they are well out of the way  where people are walking, ideally bordering the perimeter of the room - setting up the HTC Vive in a corner can help with this.

## Setting up lighthouses 
In order to set up a lighthouse, you will need four components: an HTC Vive lighthouse, an HTC Vive lighthouse power adapter (long lead), a camera stand and a camera ball mount. 

To set up a lighthouse for use, do the following -
1. Extend the legs of the camera stand and adjust the height so it’s roughly at shoulder level for an average-sized person. 
2. Place the camera stand in the position where you want the lighthouse to be. 
3. Attach the camera ball mount to the stand.
4. Attach the lighthouse to the camera ball mount. 
5. Make sure that the lighthouse is pointing towards the other lighthouse. Adjust the lighthouse orientation so that it is pointing across the floor of the space.
6. Using the HTC Vive lighthouse power adapter (long lead), plug the lighthouse into the mains.
7. Set the channel of the lighthouse. You can see what channel the lighthouse is set to by peering into the bottom left corner of the lighthouse (from the front): one lighthouse should be on channel "B", and the other on channel "C". This can be adjusted by pressing the button on the back of the lighthouse. 
7. Adjust the height of the stand so the lighthouses are 2m high.

Once both lighthouses are set up, you need to check that the lighthouses can ‘see’ one another - this will be indicated by a green light on each box. If the light is shining blue, this means the boxes are still searching. Wait patiently for a few seconds, then try adjusting the position, height and tilt of the lighthouses, and double check the channels are set correctly. 

## Connecting the HTC Vive to your Computer


In order to connect your HTC Vive to your computer, you will need several components: An HTC Vive headset, an HTC Vive adapter box, an HTC Vive headset power adapter (short lead), an HDMI cable, and a USB to USB lead. 

To set up the headset for use, do the following - 
1. On the ORANGE side, plug the HTC Vive headset into the HTC Vive adapter box. The three leads which trail from the headset should have their own respective ports. 
2. Using an HDMI lead, from the NON-ORANGE side, plug the adapter box into your computer’s HMDI out port.
3. Using a USB to USB lead, from the NON-ORANGE side, plug the adapter box into your computer’s USB port. 
4. Using the HTC Vive headset power adapter (short lead), from the NON-ORANGE side, connect the headset to the mains. 

**Note:** If you are using the HTC Vive Pro, you have to press the button on the adapter to turn the headset on. 

Once the headset is ready, locate the two controllers and check they have sufficient charge by turning them on. If needed, charge them by connecting them to your computer’s USB port via a micro USB lead. 

## Opening SteamVR

Narupa runs via the SteamVR interface, and so requires the HTC Vive to be set up in the SteamVR program prior to use.

On the desktop, double click the Steam icon (alternatively, click the windows icon in the bottom left and type Steam into the search bar). Log into Steam.

Click the VR button in the top right corner. 

<img src="resources/steam.png" alt="Steam VR button." style="width: 300px;"/>

A small pop-up will appear on the screen (see below). Check the components of VR are working correctly. This is indicated by each of the five symbols (representing the headset, two controllers, and two lighthouses) being green. 

The most common reason for a component not working correctly is it isn’t in the field of view of the lighthouses - simply moving the controllers or headset will likely fix the problem. 

You may also run into the infamous "Compositor is not running" message. In our experience, one needs to exit SteamVR and launch it again to fix this. 

<img src="resources/steamvr.png" alt="Steam VR interface." style="width: 300px;"/>

Click the arrow next to steam VR and select room setup:


<img src="resources/steamvr_dropdown.png" alt="Steam VR Dropdown." style="width: 300px;"/>
<img src="resources/steamvr_roomsetup.png" alt="Steam VR Room Setup." style="width: 300px;"/>

Follow the setup steps (it is fairly self-explanatory) and your HTC Vive will be set up and ready to go! If performing a multiplayer setup, we have found that we get the best results if when calibrating the floor, the controllers are placed in the same position for all players. 

## Multiplayer Setup

Multiplayer setup is effectively the same as single player, except with the additional requirement of connecting to the network. The following instructions are for setting up multiplayer in the same shared physical space: 
1. For each player, setup a machine with an HTC Vive, following the instructions above. All machines should share the same pair of base stations. 
2. Connect all of the machines to a shared LAN network by plugging an Ethernet cable from each machine to the router. Note: Do not plug into the port labelled as internet. 
3. Make sure each machine is not connected to another Wifi network (e.g. eduroam or whatever). 
5. Run the front end on each machine, and connect to the server you’re running. 
6. You should be able to see each player’s headsets and controllers in VR. If things appear to be flipped for a player, press the BACKSPACE button on that machine.

@section usingNarupaVR Using Narupa XR 

When the application is first launched, you will be presented with a main menu screen in the virtual space: 

<img src="resources/narupa_vrmainmenu.png" alt="Narupa XR Main Menu." style="width: 300px;"/>

From here, you can connect to a simulation running on the local network, or configure settings by pointing the controller with the laser pointer on it at the button and pulling the trigger.

Upon clicking browse servers, you will be presented with all the servers running on the local network. 

**Troubleshooting** 

If there no servers are appearing, check that you are on the same network as the server you are expecting to see. On Windows, ensure that the local network you are on is configured to be a Private network. 

In the options menu, you can change the following settings: 

* Local Multiplayer: If enabled, the simulation box will be aligned such that all users share the same experience. 
* User Mode: If set to novice mode, then you will have basic UI features. These include switching between a few preset renderers, and pausing, playing and resetting the simulation. 
* Lighthouse Indexes: If local multiplayer is enabled, the lighthouses are used to coordinate the space for all players. There are 16 device indexes used by SteamVR devices (including the headset, controllers and other trackers). The app should automatically correctly identify the lighthouses. However, if this fails, you can use adjust the lighthouse indexes until the multiplayer looks right (with the simulation box in the middle of your space, and other players being rendered in their physical locations). 
* Enable Audio: If enabled, this turns on Unity's Audio engine. The corresponding flag can be found in the editor by going to edit/project_settings/audio.

Once you are connected to a server, the main menu will also present the option to load new simulations. Within this menu, there is a selection of preconfigured demos that you can run. 

## Advanced Mode 

In advanced mode, a whole suite of additional features become available. These are centered around the concept of layers. Instead of a single rendering mode, and only grabbing atoms one at a time, you can create layers out of subsets of the atoms, and customize how they look and how you interact with them. 

The controller UI takes on a different appearance in advanced mode. At the top of the panel, you can switch from Interaction Mode to Selection Mode. In Interaction Mode, the simulation is running and you can manipulate atoms. In Selection Mode, the simulation is paused and you can select atoms to create new layers. Upon pressing Selection Mode for the first time, a new empty selection is created, and the panel switches to the following layout: 

<img src="resources/narupa_vrselection_mode.png" alt="Selection Mode." style="width: 300px;"/>

There are a number of tools to facilitate the selection of atoms. Atoms can be selected by reaching towards them in the space and pulling the trigger, and can be selected as groups or chains. There are also menus for selecting by residue number, or just backbone atoms. 

By clicking on the Layers Menu button, you can add, remove and configure all the layers you have created: 

<img src="resources/narupa_vrlayers_menu.png" alt="Layers Menu." style="width: 300px;"/>

There is a base layer which represents the default visualisation layer that cannot be removed. It can however, be hidden or adjusted. 

By selecting a layer within this menu, you can adjust the style and colour with which it is represented, and change how you interact with it. The following interaction modes are available: 

* None - This layer is not interactable. 
* Atom - Interaction forces are applied to single atoms. 
* Group - Interaction forces are applied to the whole selection as a group. The center of mass of the group of atoms is used as the target of the interaction. This mode allows you to move whole groups of atoms while minimally affecting the structure. 

