
This website documents the Narupa XR client of Narupa platform, for both users and developers. 

To get started with using Narupa XR, check out the [user guide](UserGuide/UserGuide.md).

To contribute to development, head over to the [developer guide](DeveloperGuide/DeveloperGuide.md).
