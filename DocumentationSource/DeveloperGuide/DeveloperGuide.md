# Developer Guide

This page is a guide for developers seeking to extend the Narupa XR front-end.

@section style Style Guidelines 

The following conventions are used in the codebase and should be followed to maintain consistency. It is recommended to use plugins for your IDE to simplify conforming to these standards. We use Rider and Resharper. 

* Naming Conventions: 
    * Methods, Class Names, Public Fields and Public Properties: `UpperCamelCase`. 
    * Private fields, local variables: `lowerCamelCase`. 
    * Namespaces: Should match the folder layout e.g. `Assets.Narupa.VR.Control` 
* Documentation: All public classes, fields and methods should have XML documentation. Code examples are encouraged. 

@section documentation Contributing to the Documentation 

One of the easiest and most useful contributions to make is to edit and add to this documentation. 

## Building the Documentation 

To build the documentation the following software is required: 

* Doxygen (>1.8)

There are two scripts in the root of the repository, `build_docs.sh` and `build_docs.bat`. The shell script is for Linux, Mac OS X users and Windows users with a shell (such as git-bash), while the BAT script is for Windows. 
Run the appropriate script for your system, and the documentation will be generated in the folder Documentation.

## Documentation Structure

The documentation is generated from XML comments on the code, as well as Markdown files for the main page and guides.
The documentation has the following structure: 

~~~
DocumentationSource
+-- Doxyfile
+-- UserGuide
|   +-- UserGuide.md
+-- DeveloperGuide
|   +-- DeveloperGuide.md
~~~

Additional pages can be added by editing the `INPUT` field of the `Doxyfile`. The look of the documentation page can be changed by editing the `doxygen-narupa.css` style.

@section devstructure Project Structure

The Unity3d project has the following structure: 

<img src="resources/narupa_project.png" alt="NarupaXR Project" style="width: 300px;"/>

The main folders of interest are the NSB folder, which contains the core classes and prefabs for creating a Narupa client, and Narupa.VR, which contains all the classes and prefab for the  VR build. 

The default scene for VR is NarupaVR, in the Narupa.VR/Scenes folder. 

This scene as the following heirarchy, shown below: 

<img src="resources/narupa_heirarchy.png" alt="NarupaVR heirarchy" style="width: 300px;"/>

The purpose of each component of the heirarchy is described in order below:

* `MultiplayerCalibration`: Classes and objects for calibrating local multiplayer sessions. 
* `WorldSpaceUI`: Classes and objects for UI canvases displayed in world space: 
    * `MainMenu`: The main menu canvas, for connecting to servers, loading new simulations and configuring options. 
    * `SelectionSwitchManager`: The canvas for switching between and configuring layers. 
    * `AtomToolTipManager`: The canvas for displaying atom tooltips. 
* `AppController`: Classes for controlling user experience through the app. 
* `NSB Playback`: The Narupa playback object. Contains all things molecular. 
    * `Styles`: A set of \ref NSBStyle objects that are used by renderers to colour and size atoms. 
    * `Selections`: Game objects for editor based selections (not to be used in conjunction with layers)
    * `SelectionManager`: The game object for handling the creation and spawning of new layers in Advanced Mode. 
    * `Scene Scaling`: The root transform of the molecular view. Anything within this game object is in Narupa units (aka nm). This object contains all the renderers and is where the action happens. 
    * `SteamVRRenderer`: Manages the renderering of multiplayer avatars. 
    * `SteamVRNarupaSender`: Manages the transmission of the local player for multiplayer. 
* `[VRTK_SDKManager_Narupa]`: The VRTK SDK manager, that handles setting up the VR rig. 
* `MaterialColor`: A game object for automating the colours of UI in the scene using the Android Material specification. 
* `NarupaVRControllers`: The VR controller models, UI and scripts for interactions. 

@section data Accessing Simulation Data 

For the purposes of manipulating and visualising a simulation, the most important component is the \ref Narupa.VR.Player.Control.VrPlaybackRenderer "VrPlaybackRenderer" which handles connecting to a simulation, recording frames and driving the renderer. This component is attached to the `Playback` object. The next most important component is the \ref NSBFrameReader "NSBFrameReader" which handles the recording of trajectory data into a convenient data structure, a \ref NSBFrame "NSBFrame". This class is constructed by the `VrPlaybackRenderer`. The NSBFrame should contain all the molecular data needed for most applications. 

The latest frame can be accessed conveniently using the \ref FrameFilter class. This class takes a \ref IFrameSource (supplied by linking in the editor) and can be configured to perform something with that simulation frame every Unity frame. The following example uses this structure to sonify the kinetic energy of the system:

~~~{.cs}
using System;
using Nano.Transport.Variables;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KineticEnergySonification : FrameFilter
{
    [SerializeField]
    private AudioSource audioSource;
    [SerializeField]
    private AudioLowPassFilter lpf;
    [SerializeField]
    private float filterLow;
    [SerializeField]
    private float filterHigh;
    [SerializeField]
    private float rampSpeed;
    private float previousEnergy;
    private float deltaEnergy;
    private float currentPitch;
    private float destinationPitch;
    private float energy;
    private float t = 0.0f;
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        previousEnergy = deltaEnergy = 0;
        rampSpeed = 0.5f;
        filterLow = 100;
        filterHigh = 8000;
        currentPitch = 0;
        destinationPitch = 0;
    }

    public override void Refresh()
    {
        var frame = frameSource.Component.Frame;
        energy = 0;
        if (frame == null)
            return;
        try
        {
            energy = frame.GetVariable<float>(VariableName.KineticEnergy);
        }
        catch (Exception e)
        {
        }
        deltaEnergy = energy - previousEnergy;
        previousEnergy = energy;
        float deltaEnergyAbs = Mathf.Abs(deltaEnergy);

        if (deltaEnergy <= 1000)
        {
            float deltaEnergyNorm =  deltaEnergy / 500.0f;
            float cutoff = (deltaEnergyNorm * filterHigh) + filterLow;
            Debug.Log("deltaEnergyNorm: " + deltaEnergyNorm);
            destinationPitch = cutoff;
        }
        // Update the current pitch according the destination and the rampSpeed
        if (currentPitch != destinationPitch)
        {
            currentPitch = Mathf.Lerp(currentPitch, destinationPitch, Time.deltaTime * rampSpeed);
            lpf.cutoffFrequency = currentPitch;
        }
    }
}
~~~

To use the script, add it as a component to a game object in the scene, and drag the `NSB Playback` object to the `Frame Source` field. 

@section rendering Molecular Aesthetics

Narupa XR provides a flexible framework for rendering molecules. The core base class is the \ref MMDRenderer. It works by being given a \ref NSBFrame, a \ref NSBSelection and a \ref NSBStyle. The \ref NSBFrame provides the MMDRenderer with the molecular data for the current frame, including atom positions, bonds, and elements. The \ref NSBSelection can be used to control what subset of atoms the MMDRenderer is applied to, enabling layering of renderers. Finally, the \ref NSBStyle is a set of style guidelines for the renderer to follow, consisting of element colours and radius scale factors. The \ref RendererManager class provides an example of how one can drive these renderers within a scene.

An MMDRenderer should also implement a \ref RendererConfig, which provides an interface for configuring the renderer at runtime. For example, the \ref VDWRepresentation uses the \ref RendererConfig to expose the following settings in the editor: 

<img src="resources/narupa_vdwrepresentation.png" alt="VDW Representation Settings" style="width: 300px;"/>

MMDRenderers can be composed by combining other renderers with different settings. For example, the \ref Narupa.VR.Renderers.NSBOutline renderer consists of a \ref VDWRepresentation for the atom spheres, a \ref PointsRepresentation for the atom outlines, a \ref BondsRepresentation for the bonds and a \ref LinesRepresentation for the bond outlines.

## Performance Notes

Rendering lots of atoms in VR is expensive. The expense can be due to a combination of CPU pressure in manipulating the simulation data as well as the pure rendering cost. For VR applications, the target framerate is 90 FPS. If the framerate starts to dip below that, consider the following: 

* Are the \ref NSBStyle "NSBStyles" in your scene set to AutoRefresh every frame? This can be expensive. 
* Try to reduce the amount of data processing you are doing each frame. Combine renderers into one script where possible. 
* Reduce the polygon count. The core renderers, \ref VDWRepresentation and \ref BondsRepresentation, include settings for lowering the polygon quality. 
* Lower the quality settings such as shadows.  

@section transport Communicating with the Narupa Engine

**Note:** The communication layer will soon be undergoing an overhaul to make it easier to use.

The Unity3d project is essentially just a visualization of the simulation data provided by the Narupa Engine. Data from the server takes three forms, Streams, Variables and OSC messages. Streams are used to transmit bulk data such as atom positions and interactions. Variables are essentially OSC messages communicating important simulation properties such as potential energy and temperature, wrapped up in an easily accessible manner. OSC messages are a generic transport protocol that can be used to control the behaviour of the server. The \ref NSBFrameReader class and the \ref Nano.Core.Client class demonstrate several examples of using all these data types. Below, we provide examples of setting up new communication routines. 

## Subscribing to a Stream 

Suppose you have added a new stream to the Narupa Engine (please read the section of the Narupa Engine documentation for examples on how to do this). A stream can (currently) only transmit arrays of structs of a predetermined size and type. Let us suppose the new stream transmits `float` values representing the kinetic energy of each atom. To subscribe to this stream, you will need to add it to the \ref NSBFrameReader class, using the `StreamID` to identify it. 

Declare the new stream: 

~~~{.cs}
    private SimboxSyncBuffer<float> atomKineticEnergy = new SimboxSyncBuffer<float>((float) StreamID.AtomKineticEnergy);
~~~

In the `AttachClient` method, add your stream to be attached: 

~~~{.cs}
    atomKineticEnergy.Attach(client);
~~~

In the `DetachClient` method, add your stream to be detached: 

~~~{.cs}
    atomKineticEnergy.Detach(client);
~~~
The stream will now be set up. To make it more convenient to use, you will probably want to add it to the \ref NSBFrame class to have it be recorded each frame. 

## Subscribing to an OSC Message

Suppose you have added a new OSC message to the server to indicate that something has happened, for example that a particular bond has broken (please read the relevant section of the Narupa Engine documentation for examples on how to do this), with address `/sim/bondBroken`. You will need to subscribe to the address of the message once the \ref VrPlaybackRenderer has connected the simulation. You can use the \ref VrPlaybackRenderer.OnReady event to achieve this. Below is an example `MonoBehavior` class:

~~~{.cs}

private bool bondBrokenReceived = false; 

private void Start() 
{
    var playback = FindObjectOfType<VrPlaybackRenderer>();
    playback.OnReady += OnReady;
} 

private void OnReady() 
{
    playback.Reader.Client.Attach("/sim/bondBroken", OnBondBrokenOscMessage);
}

private void OnBondBrokenOscMessage(OscMessage message)
{
    bondBrokenReceived = true;
    Debug.Log("Bond broken message received!");
}

private void Update()
{
    if(bondBrokenReceived)
    {
        //Do something with that information. 
        bondBrokenReceived = false;
    }
}

~~~

Note that the message event will be triggered outside of the main Unity thread. Many Unity functions such as activating GameObjects can only happen in the main thread, so you may need to wait until the next frame to respond to the message (as in the example above).

## Transmitting an OSC Message 

Suppose that you have added a new OSC message based command to the server (see OscCommands in the Narupa documentation). A real example is to remove all the restraints on a particular atom with the address "/restraints/removeRestraints", which takes an index of an atom as an argument. The example class below transmits this command: 

~~~{.cs}
public class RestraintCreator : MonoBehaviour { 
 
 
    private VrPlaybackRenderer playback; 
 
    // Use this for initialization 
    void Start() 
    { 
        playback = FindObjectOfType<VrPlaybackRenderer>(); 
    } 
 
    public void SendRemoveRestraint(int index) 
    { 
        if (playback != null && playback.Reader != null && playback.Reader.Ready) 
        { 
            playback.Reader.Client.Send(new OscMessage("/restraint/removeRestraints", index)); 
        } 
 
    } 
   
} 
~~~

## Broadcasting an OSC Message To All Clients

To broadcast a message to all clients, transmit a message to the server with the prefix "/broadcast". The server will forward it on to all clients. 

~~~{.cs}
public class ClientBroadcaster : MonoBehaviour { 

    private string broadcastAddress = "/broadcast/example";
    
    private void AttachToBroadcast(Client client)
    {
        client.TransportContext.OscManager.Attach("/broadcast/example", OnBroadcastReceived);
    }

    private void SendBroadcast() 
    { 
        if (playback != null && playback.Reader != null && playback.Reader.Ready) 
        { 
            playback.Reader.Client.Send(new OscMessage("/restraint/removeRestraints", index)); 
        } 
    } 
   
    private void OnBroadcastReceived(OscMessage message)
    {
        Debug.Log("Broadcast received.");
    }
  
} 
~~~

### Style Guidelines: 

To make the UI consistent, the following style guidelines should be followed: 

* There should be a \ref Narupa.VR.MaterialPalette "MaterialPalette" object in the scene, which controls the colour scheme for UI elements in a manner similar to Android's Material design. 
* Buttons, Toggles, and background canvases should make use of the \ref Narupa.VR.MaterialPalette.ButtonMaterialPaletteColorPicker "ButtonMaterialPaletteColorPicker", the \ref Narupa.VR.MaterialPalette.ToggleMaterialPaletteColorPicker "ToggleMaterialPaletteColorPicker" and the \ref Narupa.VR.MaterialPalette.LayerMaterialPaletteColorPicker "LayerMaterialPaletteColorPicker" to automate handling of colours. 
* All text should use TextMeshPro objects, and should use the Roboto-Bold fonts. Titles should use the Drop Material preset with this font. 
* Ensure that text is big enough to be read in VR, and consistent with VR menus of a similar size / distance from the user.