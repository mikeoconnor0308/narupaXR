﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using Narupa.VR.MaterialPalette;
using Narupa.VR.Multiplayer.PlayerMaterials;
using UnityEngine;

namespace Narupa.VR.Controller
{
	/// <summary>
	/// 	Assigns the correct colors to the Vive controller model
	/// </summary>
	public class VrViveModelMaterialPaletteColorPicker : MaterialPaletteUser {

		[SerializeField] private VrViveModel viveModel;
		
		[SerializeField] private MultiplayerObjectMaterialController objectMaterialController;

		[SerializeField] private VrControllerStateManager controllerStateManager;

		protected override void Awake()
		{
			base.Awake();
			if (!RefreshReferences())
				this.enabled = false;
			objectMaterialController = GetComponent<MultiplayerObjectMaterialController>();
			if(objectMaterialController != null)
				objectMaterialController.PaletteUpdated += ObjectMaterialControllerOnPaletteUpdated;
			controllerStateManager = FindObjectOfType<VrControllerStateManager>();
			controllerStateManager.ModeStart += ControllerStateManagerOnModeStart;
		}

		public override void UpdateColors()
		{
			if (!RefreshReferences())
				return;
			var palette = GetPalette();
			if (palette == null) return;
			viveModel.UpdateColors(palette.PrimaryColor, palette.SecondaryColor);
		}

		private bool RefreshReferences()
		{
			if (viveModel == null)
				viveModel = GetComponentInChildren<VrViveModel>();
			return viveModel != null;
		}

		private void ControllerStateManagerOnModeStart(object sender, VrControllerStateManager.ControllerModeChangedEventArgs e)
		{
			if (!RefreshReferences())
				return;
			viveModel.UpdateBodyColors(e.NewMode.PrimaryColor, e.NewMode.PrimaryColorBody);
		}

		private void ObjectMaterialControllerOnPaletteUpdated(object sender, MultiplayerObjectMaterialController.ColorPaletteUpdatedEventArgs e)
		{
			if (!RefreshReferences())
				return;
			viveModel.UpdateColors(e.Primary, e.Secondary);
		}
		
	}
}
