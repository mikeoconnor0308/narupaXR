﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using Narupa.VR.UI.Radial;

namespace Narupa.VR.MaterialPalette
{
	public class RadialProgressBarColorPicker : MaterialPaletteUser
	{

		private UiRadialProgressBar progressBar;
		
		// Use this for initialization
		void Awake ()
		{
			RefreshReferences();
		}

		public override void UpdateColors()
		{
			RefreshReferences();
			var palette = GetPalette();
			progressBar.BackgroundBar.color = palette.PrimaryColor;
			progressBar.ProgressBar.color = palette.SecondaryColor;
		}

		private void RefreshReferences()
		{
			if (progressBar == null)
				progressBar = GetComponent<UiRadialProgressBar>();
		}

		private void OnValidate()
		{
			UpdateColors();
		}
	}
}
