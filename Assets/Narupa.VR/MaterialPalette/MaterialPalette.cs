﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using UnityEngine;

namespace Narupa.VR.MaterialPalette
{
    /// <summary>
    ///     Represents an Android Material color palette.
    /// </summary>
    [CreateAssetMenu(fileName = "Palette", menuName = "Material Palette")]
    public class MaterialPalette : ScriptableObject
    {
        /// <summary>
        /// Background color, dark.
        /// </summary>
        public Color BackgroundColorDark = new Color(0.2f, 0.2f, 0.2f);
        /// <summary>
        /// Background color, light.
        /// </summary>
        public Color BackgroundColorLight = new Color(0.9f, 0.9f, 0.9f);
        /// <summary>
        /// Dark version of primary color.
        /// </summary>
        public Color DarkPrimaryColor = new Color(0, 0.592f, 0.654f);
        /// <summary>
        /// Dark version of secondary color.
        /// </summary>
        public Color DarkSecondaryColor = new Color(0.215f, 0f, 0.415f);
        /// <summary>
        /// Divider color.
        /// </summary>
        public Color DividerColor = new Color(0.741f, 0.741f, 0.741f);
        /// <summary>
        /// Light version of primary color.
        /// </summary>
        public Color LightPrimaryColor = new Color(0.698f, 0.921f, 0.949f);
        /// <summary>
        /// Light version of secondary color.
        /// </summary>
        public Color LightSecondaryColor = new Color(0.607f, 0.301f, 0.796f);

        /// <summary>
        /// Primary color.
        /// </summary>
        public Color PrimaryColor = new Color(0, 0.737f, 0.831f);
        /// <summary>
        /// Primary text color.
        /// </summary>
        public Color PrimaryTextColor = new Color(1f, 1f, 1f);
        /// <summary>
        /// Secondary color.
        /// </summary>
        public Color SecondaryColor = new Color(0.486f, 0.301f, 1f);
        /// <summary>
        /// Secondary text color.
        /// </summary>
        public Color SecondaryTextColor = new Color(0.458f, 0.458f, 0.458f);

    }
}