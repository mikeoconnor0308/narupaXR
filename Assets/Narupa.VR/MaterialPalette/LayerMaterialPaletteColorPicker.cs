﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using UnityEngine;
using UnityEngine.UI;

namespace Narupa.VR.MaterialPalette
{
    /// <summary>
    ///     Uses the <see cref="MaterialPalette" /> to choose appropriate colors for a generic layer.
    /// </summary>
    public class LayerMaterialPaletteColorPicker : MaterialPaletteUser
    {
        /// <summary>
        /// Reperesents different layers in a UI canvas.
        /// </summary>
        public enum Layer
        {
            /// <summary>
            /// Background.
            /// </summary>
            Background,
            /// <summary>
            /// Header.
            /// </summary>
            Header,
            /// <summary>
            /// A divider.
            /// </summary>
            Divider,
            /// <summary>
            /// A button.
            /// </summary>
            Button,
            /// <summary>
            /// A border.
            /// </summary>
            Border,
            /// <summary>
            /// A background in VR.
            /// </summary>
            VrBackground,
            /// <summary>
            /// A border in VR.
            /// </summary>
            VrBorder
        }

        private Image image;

        [SerializeField] private Layer layer;

        // Use this for initialization
        private void Start()
        {
            UpdateColors();
        }

        /// <inheritdoc />
        public override void UpdateColors()
        {
            image = GetComponent<Image>();
            if (image == null)
                return;
            var palette = GetPalette();
            if (palette == null) return;
            switch (layer)
            {
                case Layer.Background:
                    image.color = palette.DarkPrimaryColor;
                    break;

                case Layer.Header:
                    image.color = palette.LightPrimaryColor;
                    break;

                case Layer.Button:
                    image.color = palette.SecondaryColor;
                    break;

                case Layer.Divider:
                    image.color = palette.DividerColor;
                    break;
                case Layer.Border:
                    image.color = palette.DividerColor;
                    break;
                case Layer.VrBackground:
                    image.color = palette.BackgroundColorDark;
                    break;
                case Layer.VrBorder:
                    image.color = palette.BackgroundColorLight;
                    break;
            }
        }

        private void Reset()
        {
            UpdateColors();
        }

        private void OnValidate()
        {
            UpdateColors();
        }
    }
}