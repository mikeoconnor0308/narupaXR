﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using Nano.Transport.Variables;
using Nano.Transport.Variables.Interaction;
using Narupa.Client.Network;
using Narupa.VR.Player.Control;
using Narupa.VR.Selection;
using NSB.Simbox;
using UnityEngine;
using Quaternion = SlimMath.Quaternion;
using Vector3 = SlimMath.Vector3;

namespace Narupa.VR.Multiplayer
{
    /// <summary>
    ///     Transmits steamVR device positions to the server.
    /// </summary>
    public class SteamVrObjectSender : MonoBehaviour
    {
        /// <summary>
        ///     ID of player.
        /// </summary>
        public static byte PlayerId;

        /// <summary>
        ///     Transform of VR headset.
        /// </summary>
        public GameObject Headset;

        /// <summary>
        ///     Transform of left controller.
        /// </summary>
        public GameObject LeftController;

        // for input streams...
        private NetworkManager networkManager;

        /// <summary>
        ///     Transform of right controller.
        /// </summary>
        public GameObject RightController;


        private WorldLocalSpaceTransformer narupaTransformer;

        private readonly List<VRInteraction> vrInteractions = new List<VRInteraction>();


        [Header("Debug Mode")]
        [Tooltip("Debug mode, where local player is mirrored to simulate multiplayer.")]
        [SerializeField] 
        private bool debugMode;
        [SerializeField]
        [Range(1, 5)]
        private int numberOfCopies = 1;
        [SerializeField]
        private UnityEngine.Vector3 debugOffset = new UnityEngine.Vector3(0, 0, 1);
        
        public static byte LocalPlayerID
        {
            get
            {
                string hostName = Dns.GetHostName();
                var localIP = Dns.GetHostEntry(hostName).AddressList.First(ip => ip.AddressFamily == AddressFamily.InterNetwork);

                return localIP.GetAddressBytes()[3];
            }
        }

        private void Start()
        {
            networkManager = FindObjectOfType<NetworkManager>();
            narupaTransformer = FindObjectOfType<WorldLocalSpaceTransformer>();
            PlayerId = LocalPlayerID;
        }

        private void Update()
        {
            if (Headset != null && Headset.activeInHierarchy)
            {
                var headsetInteraction = new VRInteraction
                {
                    DeviceType = (byte) VRDevice.Headset,
                    Interaction = new Nano.Transport.Variables.Interaction.Interaction
                    {
                        InteractionType = 0,
                        PlayerID = PlayerId
                    }
                };
                AddSteamVrObject(Headset.transform, headsetInteraction);
            }

            var inSelectionMode = VrAtomSelectionManager.InSelectionMode;

            if (LeftController != null && LeftController.activeInHierarchy)
            {
                var leftControllerInteraction =
                    new VRInteraction
                    {
                        DeviceType = (byte) VRDevice.LeftController,
                        Interaction = new Nano.Transport.Variables.Interaction.Interaction
                        {
                            InteractionType = (byte) InteractionType.NoInteraction,
                            PlayerID = PlayerId
                        }
                    };
                AddSteamVrObject(LeftController.transform, leftControllerInteraction);
                var leftControllerMaterial = LeftController.GetComponentInChildren<MultiplayerObjectController>();
                if (inSelectionMode == false)
                    leftControllerMaterial.SetPlayerId(PlayerId);
            }

            if (RightController != null && RightController.activeInHierarchy)
            {
                var rightControllerInteraction =
                    new VRInteraction
                    {
                        DeviceType = (byte) VRDevice.RightController,
                        Interaction =
                        {
                            InteractionType = (byte) InteractionType.NoInteraction,
                            PlayerID = PlayerId
                        }
                    };
                AddSteamVrObject(RightController.transform, rightControllerInteraction);
                var rightControllerMaterial = RightController.GetComponentInChildren<MultiplayerObjectController>();
                if (inSelectionMode == false)
                    rightControllerMaterial.SetPlayerId(PlayerId);
            }

            CopyVrInteractions();
        }


        private void AddCopies(Transform originalTransform, VRInteraction originalInteraction, int numCopies, UnityEngine.Vector3 offset)
        {
           
            UnityEngine.Vector3 copyOffset = UnityEngine.Vector3.zero;
            var originalPos = originalTransform.position;
            for (int i = 0; i < numCopies; i++)
            {
                copyOffset += offset;
                VRInteraction offsetInteraction = new VRInteraction()
                {
                    DeviceType = originalInteraction.DeviceType,
                    Interaction = originalInteraction.Interaction,
                    Quaternion = originalInteraction.Quaternion
                };
                offsetInteraction.Interaction.PlayerID = (byte) (PlayerId + i + 1);
                var pos = narupaTransformer.TransformWorldSpacePointToNarupaSpace(originalPos + copyOffset);
                offsetInteraction.Interaction.Position = new Vector3(pos.x, pos.y, pos.z);
                vrInteractions.Add(offsetInteraction);
            }
            
        }
        
        // Transforms a steamvr from world space to narupa space, ready for transmitting to server.
        private void AddSteamVrObject(Transform steamVrTransform, VRInteraction vrInteraction)
        {
            
            var vec = narupaTransformer.TransformWorldSpacePointToNarupaSpace(steamVrTransform.position);
            var quaternion = narupaTransformer.TransformWorldSpaceQuaternionToNarupaSpace(steamVrTransform.rotation);
            vrInteraction.Interaction.Position = new Vector3(vec.x, vec.y, vec.z);
            vrInteraction.Quaternion = new Quaternion(quaternion.x, quaternion.y, quaternion.z, quaternion.w);
            vrInteractions.Add(vrInteraction);
            if(debugMode)
                AddCopies(steamVrTransform, vrInteraction, numberOfCopies, debugOffset);
        }

        /// <summary>
        ///     Called once per frame, copies the player's avatar into the <see cref="NSBFrameReader" /> to be sent to the server.
        /// </summary>
        private void CopyVrInteractions()
        {
            networkManager.CurrentConnection?.UpdateVRInputs(vrInteractions);
            vrInteractions.Clear();
        }
    }
}