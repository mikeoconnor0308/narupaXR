﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using NSB.MMD;
using NSB.MMD.Base;
using UnityEngine;

namespace Narupa.VR.Renderers
{
    /// <summary>
    /// Renders atoms in ball and stick, with outlines.
    /// </summary>
    public class NsbOutline : MMDRenderer
    {
        [SerializeField] private PointsRepresentation atomOutlines;

        [SerializeField] private LinesRepresentation bondOutlines;

        [Header("Setup")] [SerializeField] public NSBRepresentation NsbRepresentation;

        /// <summary>
        /// Settings for this renderer.
        /// </summary>
        public NsbConfig Settings = new NsbConfig();

        public override RendererConfig Config => Settings;

        /// <inheritdoc />
        public override void Refresh()
        {
            NsbRepresentation.Frame = Frame;
            NsbRepresentation.Selection = Selection;
            NsbRepresentation.Style = Style;

            NsbRepresentation.Refresh();

            atomOutlines.Frame = Frame;
            atomOutlines.Selection = Selection;

            bondOutlines.Frame = Frame;
            bondOutlines.Selection = Selection;

            if (Settings.ShowOutlines.Value)
            {
                atomOutlines.Refresh();
                bondOutlines.Refresh();
            }
        }

        /// <inheritdoc />
        public override float GetAtomBoundingRadius(int id)
        {
            return atomOutlines.GetAtomBoundingRadius(id);
        }

        /// <inheritdoc />
        [Serializable]
        public class NsbConfig : RendererConfig
        {
            public BoolParameter ShowOutlines = new BoolParameter();

            protected override void Setup()
            {
                ShowOutlines = AddBool("Show Outlines", true);
            }
        }
    }
}