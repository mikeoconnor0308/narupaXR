﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using Narupa.VR.Player.Control;
using Narupa.VR.Player.Screens.Browsing;
using UnityEngine;

namespace Narupa.VR.Player.Screens.Main
{
    /// <summary>
    /// Class for controlling transitions in the main menu UI.
    /// </summary>
    public class MainMenuController : MonoBehaviour
    {
        [SerializeField]
        private BrowsingUiController browsingUi;

        [SerializeField] private GameObject loadSimulationButton;

        [SerializeField] private GameObject mainMenuRoot;

        [SerializeField] private VrPlaybackRenderer playback;

        private void Awake()
        {
            if (browsingUi == null) browsingUi = GetComponentInChildren<BrowsingUiController>();
        }

        private void Start()
        {
            browsingUi.gameObject.SetActive(false);
        }

        private void OnEnable()
        {
            playback = FindObjectOfType<VrPlaybackRenderer>();
            if (playback.Ready)
                loadSimulationButton.gameObject.SetActive(true);
            else
                loadSimulationButton.gameObject.SetActive(false);
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (mainMenuRoot.gameObject.activeInHierarchy)
                    EnableMainMenu(false);
                else
                    EnableMainMenu();
            }
        }

        /// <summary>
        /// Enters the server selection screen. 
        /// </summary>
        public void EnterServerSelection()
        {
            HideAll();
            browsingUi.gameObject.SetActive(true);
            browsingUi.EnterServerSelection();
        }

        /// <summary>
        /// Enters the simulation selection screen.
        /// </summary>
        public void EnterSimulationSelection()
        {
            HideAll();
            browsingUi.gameObject.SetActive(true);
            browsingUi.EnterSimulationSelection();
        }

        /// <summary>
        /// Enters the direct connection screen.
        /// </summary>
        public void EnterDirectConnection()
        {
            HideAll();
            browsingUi.gameObject.SetActive(true);
            browsingUi.EnterDirectConnection();
        }

        /// <summary>
        /// Enters the options panel screen.
        /// </summary>
        public void EnterOptionsPanel()
        {
            HideAll();
            browsingUi.gameObject.SetActive(true);
            browsingUi.EnterOptionsPanel();
        }

        
        /// <summary>
        /// Quits to desktop.
        /// </summary>
        public void QuitToDesktop()
        {
            Application.Quit();
        }

        private void HideAll()
        {
            mainMenuRoot.SetActive(false);
        }

        /// <summary>
        /// Enables the main menu screen.
        /// </summary>
        /// <param name="enable">Whether to show or hide the main menu.</param>
        public void EnableMainMenu(bool enable = true)
        {
            mainMenuRoot.SetActive(enable);
            if (browsingUi == null) browsingUi = FindObjectOfType<BrowsingUiController>();
            if (browsingUi != null) browsingUi.gameObject.SetActive(false);
        }
    }
}