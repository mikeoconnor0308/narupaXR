﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Collections.Generic;
using Nano.Client;
using Narupa.Client.Network;
using Narupa.VR.Player.Control;
using Narupa.VR.Player.Screens.Browsing;
using NSB.Utility;
using UnityEngine;

namespace Narupa.VR.Player.Screens.Server
{
    /// <summary>
    /// Represents the VR server selection screen.
    /// </summary>
    public class VrServerSelectionPanel : MonoBehaviour
    {
        private BrowsingUiController browsingUi;

        private readonly object knownServersLock = new object();

        private InstancePool<SimboxConnectionInfo> servers;

        [SerializeField] private InstancePoolSetup serversSetup;

        private NetworkManager networkManager;

        /// <summary>
        /// Selects the given server.
        /// </summary>
        /// <param name="info">The server.</param>
        public void SelectServer(SimboxConnectionInfo info)
        {
            networkManager.JoinServer(info);
            browsingUi.HideBrowsingUi();
        }

        private void Awake()
        {
            servers = serversSetup.Finalise<SimboxConnectionInfo>(false);

            browsingUi = FindObjectOfType<BrowsingUiController>();

            networkManager = FindObjectOfType<NetworkManager>();
            networkManager.ServerListChanged += NetworkManagerOnServerListChanged;
        }

        private void NetworkManagerOnServerListChanged(IEnumerable<SimboxConnectionInfo> knownServers)
        {
            lock (knownServersLock)
            {
                servers.SetActive(knownServers);
            }
        }

        private void OnEnable()
        {
            networkManager.StartSearch();
        }

        private void OnDisable()
        {
            networkManager.EndSearch();
        }
    }
}