﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using UnityEngine;
using UnityEngine.Audio;

namespace Narupa.VR.Player.Screens.Options
{
    /// <summary>
    /// Toggle for enabling audio.
    /// </summary>
    [RequireComponent(typeof(OptionUiCycler))]
    public class AudioSettings: MonoBehaviour
    {
        [SerializeField] private bool AudioEnabled = false;

        private OptionUiCycler optionCycler;

        [SerializeField] private string optionLabel = "Audio Settings";

        private void Awake()
        {
            optionCycler = GetComponent<OptionUiCycler>();

            optionCycler.OptionChanged += EnableUnityAudio;

            AudioEnabled = false;

            EnableAudio(false);

            var option = AudioEnabled;

            optionCycler.RegisterBooleanOption(optionLabel);

            optionCycler.SetOption(OptionUiCycler.GetBoolOptionString(option));
        }

        private void Start(){}

        /// <summary>
        /// En/disables unity audio.
        /// </summary>
        /// <param name="enable">Enable/disable.</param>
        private void EnableAudio(bool enable)
        {
            PlayerPrefs.SetInt(optionLabel, enable ? 1 : 0);

            // Get all audio mixer groups
            AudioMixerGroup[] groups = Resources.FindObjectsOfTypeAll(typeof(AudioMixerGroup)) as AudioMixerGroup[]; 

            foreach (AudioMixerGroup group in groups)
            {
                // find the master group (perhaps there is a way to do this without iterating over them all?)
                if (group.name == "Master")
                {
                    // call 'transition to snapshot method' which ramps the mixer group between 2 predefined states (in this case Vol up and Vol down)
                    AudioMixerSnapshot[] snapShots = { group.audioMixer.FindSnapshot("MasterOn"), group.audioMixer.FindSnapshot("MasterOff") };
                    if (!enable)
                    {
                        float[] weights = { 0, 1  };
                        group.audioMixer.TransitionToSnapshots(snapShots, weights, 2f);
                    }
                    else
                    {
                        float[] weights = { 1, 0 };
                        group.audioMixer.TransitionToSnapshots(snapShots, weights, 2f);
                    }
                }
            }
        }

        private void EnableUnityAudio(object sender, EventArgs e = null)
        {
            var optionVal = sender as string;

            if (optionVal == null) return;

            var enable = OptionUiCycler.GetBoolFromOptionString(optionVal);

            // inverted because the flag in the auido manager is to disable the audio
            EnableAudio(enable);
       
            AudioEnabled = enable;
        }
    }
}