// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using UnityEngine;

namespace Narupa.VR.Sonification.Unused
{
    [RequireComponent(typeof(AudioSource))]
    public class SilentAudioSource : MonoBehaviour
    {
        void OnAudioFilterRead(float[] data, int channels)
        {
        }
    }
}
