// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using UnityEngine;

namespace Narupa.VR.Sonification.Unused
{
    [RequireComponent(typeof(AudioSource))]
    public class ShowPlayPos : MonoBehaviour
    {
        void Start()
        {
        }

        void Update()
        {
            GetComponent<GUIText>().text = GetComponent<AudioSource>().time.ToString();
        }
    }
}
