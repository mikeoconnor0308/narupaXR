﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Collections;
using Nano.Transport.Variables.Interaction;
using NSB.Utility;
using UnityEngine;

namespace Narupa.VR.Sonification
{
    public class ForceInteractionSoundsGen : InstanceView<InteractionForceInfo>
    {
        private Vector3 center;
        private Vector3 lineVector;
        [SerializeField] private float ForceMagnitudeDisplay = 1f;
        [SerializeField] private float ChorusDepth = 0.9f;

        private float fadeCoefficient = 0.97f;
        protected float currentAmp = 0f;
        protected float destinationAmp;
        protected float currentChoursRate = 0f;
        protected float destinationChoursRate = 0f;

        public float pitch = 1.0f;
        public float pan = 0.65f;

        public AudioSource audioSource;
        public AudioChorusFilter chorus;

        private float maxForce = 0.0f;

        public bool fadeFinished = false;
        private bool fading = false;

        // Use this for initialization
        void Start()
        {
        
        }

        private void OnEnable()
        {
            audioSource.Play();
            ChorusDepth = 0.7f;
            chorus.wetMix1 = 0.7f;
            chorus.wetMix2 = 0.6f;
            chorus.wetMix3 = 0.7f;
            chorus.depth= 0.6f;
            chorus.delay = 40f;
            chorus.enabled = true;
        }

        private void Awake()
        {
            //chorus.enabled = true;
            //if (!audioSource.isPlaying) { audioSource.Play(); }
        }

        // Update is called once per frame
        void Update()
        {
            if (!audioSource.isPlaying) { audioSource.Play(); }
            if (!fading)
            {
                float forceMagnitude = config.Force.LengthSquared;

                if (forceMagnitude > maxForce) { maxForce = forceMagnitude * 1.2f; }

                center = ToUnityVec3(config.SelectedAtomsCentre);
                lineVector = ToUnityVec3(config.Position - config.SelectedAtomsCentre);

                float forceMagNorm = Mathf.Clamp01(forceMagnitude / maxForce);

                ForceMagnitudeDisplay = forceMagNorm;

                destinationAmp = forceMagNorm;

                if (currentAmp != destinationAmp)
                {
                    currentAmp = Mathf.Lerp(currentAmp, destinationAmp, Time.deltaTime * 0.5f);
                }

                audioSource.volume = currentAmp * 3.0f;
                audioSource.pitch = pitch;
                audioSource.panStereo = pan;
           
                destinationChoursRate = (forceMagNorm * 39.5f) + 0.5f;

                if (currentChoursRate != destinationChoursRate)
                {
                    currentChoursRate = Mathf.Lerp(currentChoursRate, destinationChoursRate, Time.deltaTime * 0.9f);
                }
                chorus.rate = currentChoursRate;
                chorus.depth = ChorusDepth;
            }
        }

        private Vector3 ToUnityVec3(SlimMath.Vector3 v)
        {
            return new Vector3(v.X, v.Y, v.Z);
        }

        public void StartFade ()
        {
            fading = true;
            fadeFinished = false;
            StartCoroutine(Fade());
        }

        public void StopFade()
        {
            fading = false;
            fadeFinished = false;
            StopAllCoroutines();
            //StopCoroutine(Fade());
        }

        public IEnumerator Fade()
        {
            while (currentAmp > 0.00001)
            {
                currentAmp *= fadeCoefficient;
                audioSource.volume = currentAmp;
                yield return null;
            }
            fading = false;
            fadeFinished = true;
        }
    }
}
