﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Collections.Generic;
using NSB.MMD;
using NSB.MMD.Base;
using NSB.MMD.RenderingTypes;
using NSB.MMD.Styles;
using NSB.Processing;
using UnityEngine;

namespace Narupa.VR.Selection
{
    /// <summary>
    ///     Class for highlighting possible selections.
    /// </summary>
    public class SelectionHighlighter : MonoBehaviour
    {
        private IFrameSource frameSource;
        private MMDRenderer mmdRenderer;

        private readonly HashSet<HashSet<int>> setsOfAtoms = new HashSet<HashSet<int>>();

        private NSBStyle style;
        [SerializeField] private StyleSource styleSource;

        public NSBSelection Selection { get; } = new NSBSelection();

        /// <summary>
        ///     Adds the specified set of atoms to be highlighted.
        /// </summary>
        /// <param name="atomsToHighlight">Set of atoms to highlight</param>
        public void AddAtomsToHighlight(HashSet<int> atomsToHighlight)
        {
            setsOfAtoms.Add(atomsToHighlight);
            RefreshAtomsToHighlight();
        }

        /// <summary>
        ///     Removes the specified set of atoms from those to be highlighted.
        /// </summary>
        /// <param name="atomsToHighlight"></param>
        public void RemoveAtomsFromHighlight(HashSet<int> atomsToHighlight)
        {
            if (setsOfAtoms.Contains(atomsToHighlight)) setsOfAtoms.Remove(atomsToHighlight);
            RefreshAtomsToHighlight();
        }

        /// <summary>
        ///     Clears the set of atoms to be highlighted.
        /// </summary>
        internal void Clear()
        {
            Selection.Clear();
            setsOfAtoms.Clear();
        }

        private void OnEnable()
        {
            Clear();
        }

        private void RefreshAtomsToHighlight()
        {
            Selection.Clear();
            foreach (var set in setsOfAtoms)
            foreach (var atomIndex in set)
                Selection.Add(atomIndex);
        }

        private void Awake()
        {
            mmdRenderer = GetComponentInChildren<MMDRenderer>();
            if (mmdRenderer is VDWRepresentation) (mmdRenderer as VDWRepresentation).Settings.Scaling.Value = 1.1f;
        }

        private void Start()
        {
            frameSource = GetComponentInParent<IFrameSource>();
            if (styleSource == null) style = FindObjectOfType<MaxRadiusStyle>().Style;
            else style = styleSource.Component.Style;
        }

        private void Update()
        {
            if (frameSource == null) Start();
            mmdRenderer.Frame = frameSource.Frame;
            mmdRenderer.Selection = Selection;
            mmdRenderer.Style = style;
            mmdRenderer.Refresh();
        }
    }
}