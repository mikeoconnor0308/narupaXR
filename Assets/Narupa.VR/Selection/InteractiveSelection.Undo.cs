﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Collections.Generic;

namespace Narupa.VR.Selection
{
    public partial class InteractiveSelection : IUndoable
    {
        private int currentActionIndex = -1;

        public int MaxUndo = 10;
        internal string Name;
        private readonly List<SelectionAction> recentSelectionActions = new List<SelectionAction>();

        public string GetLastActionName()
        {
            if (CanUndo() == false) return string.Empty;
            var action = recentSelectionActions[currentActionIndex];
            var numberOfAtoms = action.Atoms.Count;
            if (action.Type == SelectionAction.ActionType.AddAtoms)
                return string.Format($"Add {numberOfAtoms} atoms");
            return string.Format($"Remove {numberOfAtoms} atoms");
        }

        public string GetNextActionName()
        {
            if (CanRedo() == false) return string.Empty;
            var action = recentSelectionActions[currentActionIndex + 1];
            var numberOfAtoms = action.Atoms.Count;
            if (action.Type == SelectionAction.ActionType.AddAtoms)
                return string.Format($"Add {numberOfAtoms} atoms");
            return string.Format($"Remove {numberOfAtoms} atoms");
        }

        public bool CanUndo()
        {
            if (currentActionIndex == -1) return false;
            if (currentActionIndex >= recentSelectionActions.Count) return false;
            return true;
        }

        public bool CanRedo()
        {
            if (currentActionIndex + 1 >= recentSelectionActions.Count) return false;
            return true;
        }

        public void Undo()
        {
            var actionToUndo = recentSelectionActions[currentActionIndex];
            if (actionToUndo.Type == SelectionAction.ActionType.AddAtoms)
                RemoveHighlightedFromSelection(actionToUndo.Atoms, true);
            else
                AddHighlightedToSelection(actionToUndo.Atoms, true);
            currentActionIndex--;
        }

        public void Redo()
        {
            var actionToUndo = recentSelectionActions[currentActionIndex + 1];
            if (actionToUndo.Type == SelectionAction.ActionType.AddAtoms)
                AddHighlightedToSelection(actionToUndo.Atoms, true);
            else
                RemoveHighlightedFromSelection(actionToUndo.Atoms, true);
            currentActionIndex--;
        }

        private void AddUndoableAction(SelectionAction action)
        {
            if (currentActionIndex < recentSelectionActions.Count - 1)
                recentSelectionActions.RemoveRange(currentActionIndex + 1,
                    recentSelectionActions.Count - (currentActionIndex + 1));
            if (recentSelectionActions.Count == MaxUndo) recentSelectionActions.RemoveAt(0);
            recentSelectionActions.Add(action);
            currentActionIndex = recentSelectionActions.Count - 1;
        }

        /// <summary>
        ///     Class representing the change of state that occurs during a selection operation.
        /// </summary>
        private class SelectionAction
        {
            /// <summary>
            ///     The type of action.
            /// </summary>
            public enum ActionType
            {
                AddAtoms,
                RemoveAtoms
            }

            /// <summary>
            ///     The set of atoms involved in the action.
            /// </summary>
            public HashSet<int> Atoms;

            public ActionType Type;
        }
    }
}