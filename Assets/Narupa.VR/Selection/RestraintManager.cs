﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Linq;
using Boo.Lang;
using Narupa.Client.Network;
using Narupa.VR.Player.Control;
using NSB.Simbox;
using UnityEngine;

namespace Narupa.VR.Selection
{
    internal class RestraintManager : MonoBehaviour
    {
        private NetworkManager networkManager;

        [SerializeField]
        private float restraintForce = 2000f;
        
        private void Start()
        {
            networkManager = FindObjectOfType<NetworkManager>();
        }
        
        public void AddRestraints(InteractiveSelection selection)
        {
            networkManager.CurrentConnection?.AddRestraints(selection, restraintForce);
        }

        public void RemoveRestraints(InteractiveSelection selection)
        {
            networkManager.CurrentConnection?.RemoveRestraints(selection);
        }
        
    }
}