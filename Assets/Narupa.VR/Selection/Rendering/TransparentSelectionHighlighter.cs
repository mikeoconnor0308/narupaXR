﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using NSB.MMD.Base;
using NSB.MMD.RenderingTypes;
using NSB.MMD.Styles;
using NSB.Processing;
using UnityEngine;

namespace Narupa.VR.Selection.Rendering
{
    /// <summary>
    ///     Highlights the currently selected atoms.
    /// </summary>
    public class TransparentSelectionHighlighter : MonoBehaviour
    {
        public NSBSelection ActiveSelection;

        private IFrameSource frameSource;

        [SerializeField] [Tooltip("Style source for updating this renderer to use correct radius.")]
        private StyleSource maxRadiusStyleSource;

        private MMDRenderer mmdRenderer;

        private MaxRadiusStyle style;

        private void Awake()
        {
            mmdRenderer = GetComponentInChildren<MMDRenderer>();
            frameSource = GetComponentInParent<IFrameSource>();
        }

        private void Start()
        {
            if (maxRadiusStyleSource == null) style = FindObjectOfType<MaxRadiusStyle>();
            else style = maxRadiusStyleSource.Component as MaxRadiusStyle;
        }

        private void Update()
        {
            if (ActiveSelection == null) mmdRenderer.gameObject.SetActive(false);
            mmdRenderer.Frame = frameSource.Frame;
            mmdRenderer.Selection = ActiveSelection;
            mmdRenderer.Style = style.Style;
            mmdRenderer.Refresh();
        }
    }
}