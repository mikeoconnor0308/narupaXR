﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
namespace Narupa.VR.Selection
{
    /// <summary>
    ///     Class for storing the atom collision information.
    /// </summary>
    public class AtomColliderEventInfo
    {
        /// <summary>
        ///     ID of the atom.
        /// </summary>
        public int AtomId;

        /// <summary>
        ///     ID of the object that collided with the atom.
        /// </summary>
        public uint ColliderId;
    }
}