﻿// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using Narupa.VR.Player.Control;
using TMPro;
using UnityEngine;

namespace Narupa.VR.UI.Playback
{
	public class UiPlaybackFrameDisplay : MonoBehaviour
	{

		private VrPlaybackRenderer playbackRenderer;
		private TextMeshProUGUI textRenderer;
	
		// Use this for initialization
		private void Awake ()
		{
			playbackRenderer = FindObjectOfType<VrPlaybackRenderer>();
			textRenderer = GetComponent<TextMeshProUGUI>();
		}
	
		// Update is called once per frame
		private void Update ()
		{
			textRenderer.text = ((int)playbackRenderer.CurrentFrame).ToString();
		}
	}
}
