﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using Narupa.VR.Player.Control;
using Narupa.VR.Selection;
using Narupa.VR.UI.Selection;
using UnityEngine;

namespace Narupa.VR.UI
{
    public class KeypadController : VrToggleBase
    {
        [SerializeField] private GameObject keypadCanvas;
        private VrAppController player;
        private GameObject resultsBox;
        private SelectionManager selectionManager;


        protected override void Awake()
        {
            base.Awake();

            ToggleChangeValue += OnToggleChangeValue;
            if (player == null) player = FindObjectOfType<VrAppController>();
            selectionManager = FindObjectOfType<SelectionManager>();
            resultsBox = keypadCanvas.transform.Find("ResultsBox").gameObject;
        }

        private void OnToggleChangeValue(object obj, EventArgs e = null)
        {
            var toggledOn = (bool) obj;
            keypadCanvas.SetActive(toggledOn);
        }

        //Reset button and canvas
        private void OnDisable()
        {
            //Reset keyboardCanvas
            NumInputManager.ResetInputValue();
            keypadCanvas.transform.Find("ResultsBox").gameObject.SetActive(false);
            keypadCanvas.SetActive(false);
            Toggle.isOn = false;
        }
    }
}