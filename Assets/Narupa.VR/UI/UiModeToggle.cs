﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using Narupa.VR.Player.Screens.Options;
using UnityEngine;

namespace Narupa.VR.UI
{
	/// <summary>
	///     Toggle for switching between UI Modes
	/// </summary>
	public class UiModeToggle : MonoBehaviour
    {
	    /// <summary>
	    ///     Represents available UI modes.
	    /// </summary>
	    public enum UiMode
        {
	        /// <summary>
	        ///     Novice mode, where the user can only change default layer rendering.
	        /// </summary>
	        Novice,

	        /// <summary>
	        ///     Advanced mode, where user has access to all the layer management etc.
	        /// </summary>
	        Advanced
        }

        private static readonly string OptionLabel = "UI Mode";

        public static UiMode ActiveUiMode;

        private OptionUiCycler optionCycler;

	    /// <summary>
	    ///     Event for when UI mode changes.
	    /// </summary>
	    /// <remarks>
	    ///     Subscribe to this event to handle changes in UI that should follow a UI mode change.
	    /// </remarks>
	    public static event EventHandler UiModeChange;

        public void Awake()
        {
            optionCycler = GetComponent<OptionUiCycler>();

            optionCycler.OptionChanged += SetUiMode;

            var multInt = (int) ActiveUiMode;
            //This didn't seem to work well in editor?
            if (PlayerPrefs.HasKey(OptionLabel))

            {
                multInt = PlayerPrefs.GetInt(OptionLabel);

                ActiveUiMode = (UiMode) multInt;
            }

            optionCycler.RegisterOption<UiMode>(OptionLabel);
            SetUiMode((UiMode) multInt, false);
        }

        private void SetUiMode(object sender, EventArgs e)
        {
            var enumStr = sender as string;
            if (enumStr == null)
                throw new Exception("Failed to set UI mode, as object received was not a string");
            UiMode mode;
            var result = Enum.TryParse(enumStr, true, out mode);
            if (!result) throw new Exception("Failed to parse UI mode");
            SetUiMode(mode);
        }

	    /// <summary>
	    ///     Sets the UI mode to the given mode.
	    /// </summary>
	    /// <param name="mode"></param>
	    /// <param name="initialised">Override for the first time this is called.</param>
	    public static void SetUiMode(UiMode mode, bool initialised = true)
        {
            if (initialised && mode == ActiveUiMode) return;
            ActiveUiMode = mode;
            UiModeChange?.Invoke(mode, null);
            PlayerPrefs.SetInt(OptionLabel, (int) mode);
        }

        private void OnEnable()
        {
            optionCycler.SetOption((int) ActiveUiMode);
        }
    }
}