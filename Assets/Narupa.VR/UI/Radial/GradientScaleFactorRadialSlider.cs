﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using Narupa.VR.Interaction;
using UnityEngine;

namespace Narupa.VR.UI.Radial
{
    public class GradientScaleFactorRadialSlider : MonoBehaviour
    {
        private const string Label = "GradientScaleFactor";

        public float CurrentValue = 1f;
        private RadialSlider radialSlider;

        // Use this for initialization
        private void Start()
        {
            radialSlider = GetComponent<RadialSlider>();
            radialSlider.ValueChanged += RadialSliderOnValueChanged;
            if (PlayerPrefs.HasKey(Label))
                CurrentValue = PlayerPrefs.GetFloat(Label);
            radialSlider.SetValue(CurrentValue);
        }

        private void RadialSliderOnValueChanged(object sender, RadialSlider.SliderValueChangeEventArgs e)
        {
            CurrentValue = e.Value;
            PlayerPrefs.SetFloat(Label, CurrentValue);
            VrInteractionController.GradientScaleFactor = CurrentValue / radialSlider.MaximumValue * 3f;
        }
    }
}