﻿// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using UnityEngine;

namespace Narupa.VR.UI.Implementation
{
	public class VrAdvancedMenu : MonoBehaviour
	{

		public VrCanvas keypadCanvas;
		
		public void OpenKeypad()
		{
			GetComponent<VrContextMenu>().SwitchToCanvas(keypadCanvas);
		}
		
	}
}
