﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Collections;
using UnityEngine;

namespace Narupa.VR.UI
{
    /// <summary>
    ///     Class for UI positioned on the VR controller.
    /// </summary>
    [RequireComponent(typeof(VrContextMenu))]
    internal class VrControllerUi : MonoBehaviour
    {
        public static bool EnableTransform = true;

        [SerializeField] private VrCanvas advancedMenu;

        /// <summary>
        ///     The menu to default to when bringing up the UI.
        /// </summary>
        [SerializeField] private VrCanvas defaultMenu;

        [SerializeField] private VrCanvas noviceMenu;

        private VrContextMenu rootContextMenu;

        private bool uiModeEnabled;

        /// <summary>
        ///     Triggered when UI mode is exited by the user.
        /// </summary>
        public event EventHandler UiModeExited;

        /// <summary>
        ///     Triggered when UI mode is entered by the user.
        /// </summary>
        public event EventHandler UiModeEntered;

        private void Awake()
        {
            rootContextMenu = GetComponent<VrContextMenu>();
            UiModeToggle.UiModeChange += OnUiModeChange;
            
            OnUiModeChange(UiModeToggle.ActiveUiMode, null);
        }

        private void OnUiModeChange(object sender, EventArgs e)
        {
            var mode = (UiModeToggle.UiMode) sender;

            switch (mode)
            {
                case UiModeToggle.UiMode.Novice:
                    SwitchToNoviceMode();
                    break;

                case UiModeToggle.UiMode.Advanced:
                    SwitchToAdvancedMode();
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void Start()
        {
        }

        internal void HideUi()
        {
            EnableUiMode(false);
        }

        internal void ShowUi()
        {
            EnableUiMode(true);
        }

        /// <summary>
        ///     Enables the controller UI.
        /// </summary>
        /// <param name="enable"></param>
        public void EnableUiMode(bool enable)
        {
            uiModeEnabled = enabled;
            if (enable)
            {
                StartUiEntryAnimation();
                UiModeEntered?.Invoke(this, null);
            }
            else
            {
                rootContextMenu.HideAll();
                StartCoroutine(DisableRootCanvasRoutine(rootContextMenu.SwitchTimeDelay + 0.1f));
                UiModeExited?.Invoke(this, null);
            }
        }

        private IEnumerator DisableRootCanvasRoutine(float switchTimeDelay)
        {
            yield return new WaitForSeconds(switchTimeDelay);
        }

        private void StartUiEntryAnimation()
        {
            rootContextMenu.SwitchToCanvas(defaultMenu);
        }

        /// <summary>
        ///     Switches UI to novice mode, with simple render options.
        /// </summary>
        public void SwitchToNoviceMode()
        {
            var restoreUi = uiModeEnabled;
            defaultMenu = noviceMenu;
            if (restoreUi)
            {
                advancedMenu.gameObject.SetActive(false);
                noviceMenu.gameObject.SetActive(true);
            }
        }

        /// <summary>
        ///     Switches UI over to advanced mode, with layers and advanced interactions.
        /// </summary>
        public void SwitchToAdvancedMode()
        {
            var restoreUi = uiModeEnabled;
            defaultMenu = advancedMenu;
            if (restoreUi)
            {
                advancedMenu.gameObject.SetActive(true);
                noviceMenu.gameObject.SetActive(false);
            }
        }
    }
}