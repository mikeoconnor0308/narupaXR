﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Collections;
using NSB.Simbox.Topology;
using UnityEngine;

namespace Narupa.VR.UI.Tooltips
{
    public class AtomToolTipManager : MonoBehaviour
    {
        [SerializeField] private ProteinAtomTooltip atomTooltip;

        private uint currentMessage;

        private readonly float lerpTime = 0.1f;
        // Use this for initialization

        private Vector3 normalScale;

        private void Awake()
        {
            normalScale = atomTooltip.transform.localScale;
            atomTooltip.gameObject.SetActive(false);
        }

        private IEnumerator DisplayMessageRoutine(float time = 1f)
        {
            atomTooltip.transform.localScale = normalScale;
            if (atomTooltip.gameObject.activeInHierarchy == false)
            {
                atomTooltip.gameObject.SetActive(true);
                iTween.ScaleFrom(atomTooltip.gameObject, Vector3.zero, lerpTime);
            }

            yield return new WaitForSeconds(time);
            iTween.ScaleTo(atomTooltip.gameObject, Vector3.zero, lerpTime);
            atomTooltip.gameObject.SetActive(false);
        }

        public void HideTooltip(uint messageId)
        {
            if (messageId == currentMessage)
            {
                StopAllCoroutines();
                atomTooltip.transform.localScale = normalScale;
                atomTooltip.gameObject.SetActive(false);
            }
        }

        public uint DisplayAtomTooltip(TopologyAtom atom, float time = 2f, bool drawLine = true)
        {
            StopAllCoroutines();
            atomTooltip.SetAtom(atom);
            StartCoroutine(DisplayMessageRoutine(time));
            return currentMessage++;
        }
    }
}