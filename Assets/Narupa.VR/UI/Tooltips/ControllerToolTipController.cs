﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using UnityEngine;

namespace Narupa.VR.UI.Tooltips
{
    public class ControllerToolTipController : MonoBehaviour
    {
        private ObjectTooltip interactionCenter;
        private ObjectTooltip menuButton;
        private ObjectTooltip translateButton;
        private ObjectTooltip trigger;
        private ObjectTooltip zoomLeft;
        private ObjectTooltip zoomRight;

        public void ShowAll()
        {
        }

        public void HideAll()
        {
            menuButton.gameObject.SetActive(false);
            ;
        }

        public void ShowMenuButton(bool show)
        {
            menuButton.gameObject.SetActive(show);
        }

        public void ShowTranslateButton(bool show)
        {
            translateButton.gameObject.SetActive(show);
            zoomLeft.gameObject.SetActive(false);
            zoomRight.gameObject.SetActive(false);
        }

        public void ShowZoomButton(bool show)
        {
            zoomLeft.gameObject.SetActive(show);
            zoomRight.gameObject.SetActive(show);
            translateButton.gameObject.SetActive(false);
        }

        public void ShowTrigger(bool show)
        {
            trigger.gameObject.SetActive(show);
            interactionCenter.gameObject.SetActive(show);
        }

        // Use this for initialization
        private void Start()
        {
        }

        // Update is called once per frame
        private void Update()
        {
        }
    }
}