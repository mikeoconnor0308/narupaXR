﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using Narupa.VR.Selection;
using NSB.MMD.Base;
using TMPro;

namespace Narupa.VR.UI.RenderModes
{
    /// <summary>
    ///     Button for changing the current renderer.
    /// </summary>
    public class ChangeRenderButton : VrButtonBase
    {
        private RendererManager defaultRenderers;

        private SelectionManager selectionManager;

        private TextMeshProUGUI text;

        protected override void Awake()
        {
            base.Awake();
            text = GetComponentInChildren<TextMeshProUGUI>();
            selectionManager = FindObjectOfType<SelectionManager>();
            selectionManager.ActiveSelectionChanged += OnSelectionChange;
            FindBaseLayerRenderManager();
        }

        // Use this for initialization
        private void Start()
        {
        }

        /// <summary>
        ///     Sets this button's label to the passed label.
        /// </summary>
        /// <param name="label"></param>
        public void SetRenderLabel(string label)
        {
            text.text = label;
        }

        /// <summary>
        ///     Sets this button's label to match the active renderer.
        /// </summary>
        /// <param name="selectionRendererRoot"></param>
        public void SetRenderLabel(RendererManager selectionRendererRoot)
        {
            if (selectionRendererRoot.ActiveRenderer == null)
                SetRenderLabel(selectionRendererRoot.DefaultRenderer.name);
            else
                SetRenderLabel(selectionRendererRoot.ActiveRenderer.name);
        }

        private void OnSelectionChange(object sender, EventArgs e)
        {
            var selection = sender as InteractiveSelection;
            if (selection == null)
            {
                if (defaultRenderers != null)
                    SetRenderLabel(defaultRenderers);
            }
            else
            {
                SetRenderLabel(selection.RendererRoot);
            }
        }

        private void FindBaseLayerRenderManager()
        {
            defaultRenderers = selectionManager.BaseLayerRendererManager;
        }

        public void OnEnable()
        {
            if (defaultRenderers == null) FindBaseLayerRenderManager();
            if (selectionManager == null) selectionManager = FindObjectOfType<SelectionManager>();
            if (selectionManager.ActiveSelection == null)
                SetRenderLabel(defaultRenderers);
            else
                SetRenderLabel(selectionManager.ActiveSelection.RendererRoot);
        }
    }
}