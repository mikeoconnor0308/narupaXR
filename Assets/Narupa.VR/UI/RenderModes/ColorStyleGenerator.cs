﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Collections.Generic;
using Nano.Science;
using NSB.MMD.RenderingTypes;
using NSB.MMD.Styles;
using NSB.Processing;
using NSB.Utility;
using UnityEngine;

namespace Narupa.VR.UI.RenderModes
{
    public class ColorStyleGenerator : MonoBehaviour
    {
        [SerializeField] private Color[] colors;

        [SerializeField] private ColorStyle colorStylePrefab;
        [SerializeField] private FrameSource frameSource;

        public List<ColorStyle> ColorStyles = new List<ColorStyle>();

        // Use this for initialization
        private void Awake()
        {
            int i = 0;
            foreach (var color in colors)
            {
                var style = Instantiate(colorStylePrefab, transform);
                float h, s, v;
                Color.RGBToHSV(color, out h, out s, out v);
                style.Hue = h;
                style.Saturation = s;
                style.Value = v;
                style.gameObject.SetActive(true);
                ColorStyles.Add(style);
                style.name = "Color Style " + i;
                style.Style.Name = style.name;
                i++;
            }
        }

        //Method which allows a new NSBStyle color to be generated outside of the preset styles in ColorStyles
        public NSBStyle InitializeColor(Color32 color)
        {
            ColorStyle newColor = new ColorStyle();
            newColor.ElementColors.Resize((int)Element.MAX);

            float h, s, v;
            Color.RGBToHSV(color, out h, out s, out v);
            newColor.Hue = h;
            newColor.Saturation = s;
            newColor.Value = v;

            if (newColor.ElementRadiuses.Count == 0) newColor.ElementRadiuses.Resize((int)Element.MAX);
            newColor.RefreshElementStyles();

            NSBFrame frame = frameSource.Component.Frame;
            newColor.RefreshTopology(frame);
            newColor.Style.Name = ColorUtility.ToHtmlStringRGB(color);
            return newColor.Style;
        }
    }
}