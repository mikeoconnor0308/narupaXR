﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using NSB.MMD.Styles;
using NSB.Processing;
using NSB.Utility;
using UnityEngine;
using UnityEngine.UI;

namespace Narupa.VR.UI.RenderModes
{
    public class VrColorSetterButton : InstanceView<ColorStyle>
    {
        private Button button;

        private VrColorSetterController colorSetterController;

        private CPKStyle cpkStyle;

        [SerializeField] private bool cpkStyleButton;

        [SerializeField] private bool hueStyleButton;

        [SerializeField] private HueStyle hueStyle; 
        private Image image;

        public event EventHandler ColorStyleSelected;

        private void OnClick()
        {
            NSBStyle styleToSet;
            if (hueStyleButton)
                styleToSet = hueStyle.Style; 
            else 
                styleToSet = cpkStyleButton ? cpkStyle.Style : config.Style;

            //TODO @review how colour changes are currently handled.
            if (colorSetterController.SelectionManager.ActiveSelection == null)
                colorSetterController.SelectionManager.BaseLayerRendererManager.SetRenderStyle(styleToSet);
            else
                colorSetterController.SelectionManager.ActiveSelection.RendererRoot.SetRenderStyle(styleToSet);
        }

        private void Start()
        {
            if (cpkStyleButton)
            {
                cpkStyle = FindObjectOfType<CPKStyle>();
                button = GetComponent<Button>();
                button.onClick.AddListener(() => OnClick());
            } 
            else if (hueStyleButton)
            {
                button = GetComponent<Button>();
                button.onClick.AddListener(() => OnClick());
            }

            colorSetterController = FindObjectOfType<VrColorSetterController>();
        }

        protected override void Configure()
        {
            image = GetComponentInChildren<Image>();
            button = GetComponent<Button>();
            button.onClick.AddListener(() => OnClick());
            image.color = Color.HSVToRGB(config.Hue, config.Saturation, config.Value);
        }
    }
}