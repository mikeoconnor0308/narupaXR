﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using Narupa.VR.Selection;
using NSB.MMD.Styles;
using NSB.Utility;
using UnityEngine;

namespace Narupa.VR.UI.RenderModes
{
    public class VrColorSetterController : MonoBehaviour
    {
        private InstancePool<ColorStyle> colorSetters;

        [SerializeField] private InstancePoolSetup colorSetterSetup;

        public SelectionManager SelectionManager;


        private void Awake()
        {
            colorSetters = colorSetterSetup.Finalise<ColorStyle>(false);
        }

        private void Start()
        {
            SelectionManager = FindObjectOfType<SelectionManager>();
        }

        private void OnEnable()
        {
            var styles = FindObjectOfType<ColorStyleGenerator>();
            if (styles == null) return;
            colorSetters.SetActive(styles.ColorStyles);
        }
    }
}