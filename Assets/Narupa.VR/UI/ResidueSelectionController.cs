﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Collections.Generic;
using System.Linq;
using Narupa.VR.Selection;
using NSB.MMD.RenderingTypes;
using NSB.Simbox.Topology;
using UnityEngine;

namespace Narupa.VR.UI
{
    public class ResidueSelectionController : MonoBehaviour
    {
        [SerializeField] private FrameSource frameSource;
        private SelectionManager selectionManager;

        private void Awake()
        {
            //inputValue is modified externally by pressing numbers on the in-world keypad
            selectionManager = FindObjectOfType<SelectionManager>();
        }

        //Given a residue ID, search the NSBEnvironment and return a list of ResidueSelectData
        public List<ResidueSelectData> FindResidues(string residueId)
        {
            var residueSelectData = new List<ResidueSelectData>();
            var selectedAtoms = new HashSet<int>();
            var frame = frameSource.Component.Frame;
            var val = int.Parse(residueId);
            frame.Topology.UpdateVisibleAtomMap(frame.VisibleAtomMap);
            var atoms = frame.Topology.VisibleAtomMap;

            //Search through all visible atoms to find residues with ID which matches search
            for (var i = 0; i < atoms.Count; i++)
                if (atoms[i].GetType() == typeof(ProteinAtom))
                {
                    var pAtom = (ProteinAtom) atoms[i];
                    if (val == pAtom.ResidueId)
                    {
                        residueSelectData.Add(GetResidueData(pAtom));
                        //skip checking other atoms in residue
                        i += pAtom.Parent.Atoms.Count - 1;
                    }
                }

            //Only return residueSelectData if a relevant potential selection can be found
            if (residueSelectData.Count > 0) return residueSelectData;
            return null;
        }

        private ResidueSelectData GetResidueData(ProteinAtom atom)
        {
            //Get IDs of all residueAtoms
            var residueAtoms = new HashSet<int>();
            var residue = atom.Parent;
            foreach (ProteinAtom pAtom in residue.Atoms) residueAtoms.Add(pAtom.Index);

            //Info to return
            var allSelected = false;
            string residueInfo;

            //Check if all atoms in residue are currently selected
            if (residueAtoms.Intersect(selectionManager.ActiveSelection.Selection).Count() == residueAtoms.Count)
                allSelected = true;
            //Create unique string identifier for residue
            residueInfo = residue.Name + " (" + residue.Parent.Parent.Name + "-" + residue.Parent.Parent.Id + "-" +
                          residue.Parent.Name + ")";

            return new ResidueSelectData(residueAtoms, allSelected, residueInfo);
        }

        public void ToggleResidue(HashSet<int> residueAtoms, bool toggle)
        {
            if (residueAtoms != null)
            {
                if (toggle) selectionManager.ActiveSelection.Selection.AddRange(residueAtoms);
                else selectionManager.ActiveSelection.Selection.RemoveRange(residueAtoms);
            }
        }

        public struct ResidueSelectData
        {
            public HashSet<int> AtomIndexes { get; }
            public bool AllSelected { get; }
            public string IdString { get; }

            public ResidueSelectData(HashSet<int> atomInd, bool allSel, string id)
            {
                AtomIndexes = atomInd;
                AllSelected = allSel;
                IdString = id;
            }
        }
    }
}