﻿// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Collections;
using Narupa.VR.UI.Tooltips;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Narupa.VR.UI.Buttons
{
	
	/// <summary>
	/// 	Provides a tooltip when hovered over
	/// </summary>
	public class UiHoverTooltip : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {

		[SerializeField] protected string Message;

		[SerializeField] [Tooltip("Where to display tooltips for this button")]
		private MessageTooltipManager messageTooltip;

		[Header("Tooltip Options")] [SerializeField]
		protected string Title;
		
		public void OnPointerEnter(PointerEventData eventData)
		{
			StopCoroutine(StartTooltipDelay());
			StartCoroutine(StartTooltipDelay());
		}

		public void OnPointerExit(PointerEventData eventData)
		{
			DestroyTooltip();
		}
		
		private readonly float tooltipDelayTime = 0.2f;
		private uint toolTipId;

		private void Awake()
		{
			if (messageTooltip == null) messageTooltip = GetComponentInParent<MessageTooltipManager>();
			if (messageTooltip == null)
				Debug.LogWarning(
					"Unable to find tooltip manager in parent for this button, tooltips will not be shown.");
		}
		
		private IEnumerator StartTooltipDelay()
		{
			yield return new WaitForSeconds(tooltipDelayTime);
			if (!string.IsNullOrEmpty(Title) && messageTooltip != null)
				toolTipId = messageTooltip.DisplayTooltip(transform, Title, Message);
		}

		private void DestroyTooltip()
		{
			StopCoroutine(StartTooltipDelay());
			if(messageTooltip != null) messageTooltip.HideTooltip(toolTipId);
		}

		private void OnDisable()
		{
			DestroyTooltip();
		}
	}
}
