// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace Narupa.VR.UI.Buttons
{
    
    /// <summary>
    ///     Base wrapper for both unity UI buttons and toggle buttons. Exposes unity style events for the mouse entering
    ///     and leaving.
    /// </summary>
    public abstract class UiButtonBase : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        [System.Serializable]
        public class ButtonEventArgs : EventArgs
        {
            public UnityEngine.UI.Button Button;

            public ButtonEventArgs(UnityEngine.UI.Button button)
            {
                this.Button = button;
            }
        }
        
        [System.Serializable]
        public class ButtonEvent : UnityEvent<object, ButtonEventArgs>
        {
            
        }
        
        public ButtonEvent OnButtonEnter;
        public ButtonEvent OnButtonExit;

        protected UnityEngine.UI.Button button;

        protected virtual void Awake()
        {
            button = this.GetComponent<UnityEngine.UI.Button>();
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            OnButtonEnter?.Invoke(this, new ButtonEventArgs(button));
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            OnButtonExit?.Invoke(this, new ButtonEventArgs(button));
        }
    }
}