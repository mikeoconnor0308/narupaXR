// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using UnityEngine.Events;
using UnityEngine.UI;

namespace Narupa.VR.UI.Buttons
{
    
    /// <summary>
    ///     Wrapper for a unity toggle button, sending events for value changing as well as on or off
    /// </summary>
    public class UiToggleButton : UiButtonBase
    {
        [System.Serializable]
        public class ToggleButtonEventArgs
        {
            public UnityEngine.UI.Toggle Toggle;
            public bool Value;

            public ToggleButtonEventArgs(UnityEngine.UI.Toggle toggle, bool val)
            {
                this.Toggle = toggle;
                this.Value = val;
            }
        }
        
        [System.Serializable]
        public class ToggleButtonEvent : UnityEvent<object, ToggleButtonEventArgs>
        {
            
        }
        
        private Toggle toggle;

        public ToggleButtonEvent OnValueChanged;
        public ToggleButtonEvent OnToggleOn;

        protected override void Awake()
        {
            base.Awake();
            this.toggle = GetComponent<Toggle>();
            toggle.onValueChanged.AddListener(ToggleOnValueChanged);
        }

        private void ToggleOnValueChanged(bool active)
        {
            OnValueChanged?.Invoke(this, new ToggleButtonEventArgs(toggle, active));
            if(active)
                OnToggleOn?.Invoke(this, new ToggleButtonEventArgs(toggle, true));
        }
    }
}