﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using Narupa.VR.Controller;
using Narupa.VR.Player.Control;
using Narupa.VR.Selection;
using NSB.Utility;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Narupa.VR.UI.Selection
{
    /// <summary>
    ///     Manages the UI for creating and switching between active selections.
    /// </summary>
    public class SelectionSwitchManager : MonoBehaviour
    {
        [Tooltip("The textbox for labelling the active selection.")] [SerializeField]
        private TextMeshProUGUI activeSelectionLabel;

        private InstancePool<InteractiveSelection> availableSelections;

        [Tooltip("The default layer button.")] [SerializeField]
        private ActiveSelectionToggle defaultSelectionToggle;

        private SelectionCanvasManager selectionCanvasManager;

        private SelectionManager selectionManager;

        private VrControllerStateManager controllerStateManager;

        [Tooltip("Instance pool for spawning buttons for available selections.")] [SerializeField]
        private InstancePoolSetup selectionSetup;

        private ToggleGroup toggleGroup;

        /// <summary>
        ///     Event for when the button is clicked, and a selection is chosen.
        /// </summary>
        public event ActiveSelectionToggle.ActiveSelectionHandler SelectionChosen;

        /// <summary>
        ///     Event for when the selection button is highlighted.
        /// </summary>
        public event ActiveSelectionToggle.ActiveSelectionHandler SelectionHighlighted;

        /// <summary>
        ///     Event for when the selection button is unhighlighted.
        /// </summary>
        public event ActiveSelectionToggle.ActiveSelectionHandler SelectionUnhighlighted;

        /// <summary>
        ///     Handles when a selection is clicked.
        /// </summary>
        /// <remarks>
        ///     Upon selecting a simulation, sets the labels and triggers the event for a new selection.
        ///     If the selection chosen is <c>null</c>, then the default layer is activated.
        /// </remarks>
        /// <param name="selection">The selection to be set as the active selection, can be <c>null</c>.</param>
        /// <param name="e"></param>
        public void OnSelectionChosen(InteractiveSelection selection, EventArgs e)
        {
            if (selection != null)
            {
                activeSelectionLabel.text = $"Active: {selection.name}";
            }
            else
            {
                //if selection is null, then switch out of selection mode.
                controllerStateManager.SetModeDefault();
                activeSelectionLabel.text = "Active: Default";
            }

            SelectionChosen?.Invoke(selection, e);
        }

        /// <summary>
        ///     Handles when a selection is marked for deletion.
        /// </summary>
        /// <param name="sender">The selection to be deleted.</param>
        /// <param name="e"></param>
        public void OnSelectionDeleted(object sender, EventArgs e)
        {
            var selection = sender as InteractiveSelection;
            //If we delete the active selection, need to clear up the selection.
            if (selection == selectionManager.ActiveSelection)
            {
                selectionCanvasManager.OnSelectionCleared();
                EnableDefaultLayer(true);
            }

            if (selection != null)
            {
                RestraintManager restraintManager = FindObjectOfType<RestraintManager>();
                restraintManager.RemoveRestraints(selection);
            }

            
            selectionManager.DeleteSelection(sender as InteractiveSelection);
            availableSelections.SetActive(selectionManager.Selections);
        }

        /// <summary>
        ///     Handles selection button being highlighted.
        /// </summary>
        /// <param name="selection"></param>
        /// <param name="e"></param>
        public void OnSelectionHighlighted(InteractiveSelection selection, EventArgs e)
        {
            //TODO make it so highlighting a selection toggle highlights it
            //if (SelectionHighlighted != null) SelectionHighlighted(selection, e);
        }

        /// <summary>
        ///     Handles selection button not being highlighted any more.
        /// </summary>
        /// <param name="selection"></param>
        /// <param name="e"></param>
        public void OnSelectionUnhighlighted(InteractiveSelection selection, EventArgs e)
        {
            //TODO make it so highlighting a selection toggle highlights it.
            //if (SelectionUnhighlighted != null) SelectionUnhighlighted(selection, e);
        }

        /// <summary>
        ///     Hides the selection manager menu and cleans up.
        /// </summary>
        public void HideSelectionManager()
        {
            gameObject.SetActive(false);
            //If we're not in selection mode, hide the selection highlighter when exitting this menu.
            if (VrAppController.SelectionMode == false) selectionManager?.ShowCurrentSelection(false);

            if (selectionManager == null)
                return;
            //Save the current selection settings. 
            if (selectionManager.SelectionPathSet == false)
            {
                VrPlaybackRenderer playback = FindObjectOfType<VrPlaybackRenderer>();
                if (playback.Ready)
                {
                    string path = string.Format("{0}/SimulationSettings/{1}.json", Application.dataPath,
                        playback.SimulationName);
                    selectionManager.SaveSelections(path);
                }
                
            }
            else
                selectionManager.SaveSelections();
        }

        private void Awake()
        {
            availableSelections = selectionSetup.Finalise<InteractiveSelection>(false);
            selectionManager = FindObjectOfType<SelectionManager>();
            selectionManager.SelectionCreated += OnSelectionCreated;
            selectionCanvasManager = FindObjectOfType<SelectionCanvasManager>();
            controllerStateManager = FindObjectOfType<VrControllerStateManager>();
            toggleGroup = GetComponent<ToggleGroup>();
            defaultSelectionToggle.SelectionChosen += OnSelectionChosen;
        }

        private void Start()
        {
            gameObject.SetActive(false);
        }

        private void EnableDefaultLayer(bool enable)
        {
            var defaultToggle = defaultSelectionToggle.GetComponent<Toggle>();
            if (defaultToggle.isOn != enable)
            {
                defaultToggle.isOn = enable;
                defaultToggle.onValueChanged.Invoke(enable);
                if (enable)
                {
                    controllerStateManager.SetModeDefault();
                    activeSelectionLabel.text = "Active: Default";
                }
            }
        }

        private void OnEnable()
        {
            availableSelections.SetActive(selectionManager.Selections);
            if (selectionManager.ActiveSelection == null)
                EnableDefaultLayer(true);
            else
                EnableDefaultLayer(false);
        }

        private void OnSelectionCreated(object sender, EventArgs e)
        {
            if (gameObject.activeInHierarchy)
                availableSelections.SetActive(selectionManager.Selections);
            EnableDefaultLayer(false);
        }
    }
}