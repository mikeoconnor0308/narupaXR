﻿// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using Nano.Client;
using Narupa.VR.Player.Control;
using UnityEngine;


namespace Narupa.Client.Network
{
    /// <summary>
    /// Manages enumerating possible servers, and initialising connections once found
    /// </summary>
    public class NetworkManager : MonoBehaviour
    {
        /// <summary>
        /// Event called when a connection to a new server is attempted
        /// </summary>
        public Action<NarupaServerConnection> ServerConnectionInitialised;

        /// <summary>
        /// Event called when a new server is discovered
        /// </summary>
        public event Action<SimboxConnectionInfo> ServerDiscovered;

        /// <summary>
        /// Event called when a server expires
        /// </summary>
        public event Action<SimboxConnectionInfo> ServerExpired;

        /// <summary>
        /// Event called when the list of possible servers is changed
        /// </summary>
        public event Action<IEnumerable<SimboxConnectionInfo>> ServerListChanged;

        /// <summary>
        /// Returns a list of known servers
        /// </summary>
        public IEnumerable<SimboxConnectionInfo> KnownServers => this.knownServers;

        /// <summary>
        /// The current connection info for the server
        /// </summary>
        public NarupaServerConnection CurrentConnection => this.serverConnection;

        /// <summary>
        /// Is the client currently connected to a server
        /// </summary>
        public bool Connected => serverConnection != null;

        private SimboxConnectionInfo connectionInfo;

        private NarupaServerConnection serverConnection = null;

        private void Awake()
        {
            search = new SearchForConnections();
            search.DeviceDiscovered += OnServerDiscovered;
            search.DeviceExpired += OnServerExpired;
        }

        private void Start()
        {
            StartCoroutine(AutoconnectToServer());
        }

        private IEnumerator AutoconnectToServer()
        {
            SimboxConnectionInfo info = null;
            var search = new SearchForConnections();
            search.DeviceDiscovered += info_ => info = info_;
            search.BeginSearch();

            while (info == null)
            {
                yield return new WaitForSeconds(0.5f);

                Debug.Log("No servers found.");
            }

            search.EndSearch();

            Debug.Log($"Attempting to join server: {info.Descriptor}");

            JoinServer(info);
        }

        public void JoinServer(SimboxConnectionInfo info)
        {
            this.connectionInfo = info;
            serverConnection?.ClearConnection();
            this.serverConnection = new NarupaServerConnection(this.connectionInfo);
            this.serverConnection.ReconnectSignalReceived += OnReconnectSignalReceived;
            ServerConnectionInitialised?.Invoke(this.serverConnection);
        }

        private void ReconnectToServer()
        {
            if (this.connectionInfo == null)
                return;
            JoinServer(this.connectionInfo);
        }

        private bool shouldReconnect = false;

        private void OnReconnectSignalReceived()
        {
            shouldReconnect = true;
        }

        private void Update()
        {
            if (shouldReconnect)
            {
                ReconnectToServer();
                shouldReconnect = false;
            }

            if (discoveredServers.Count > 0 || expiredServers.Count > 0)
            {
                foreach (var discovered in discoveredServers)
                {
                    knownServers.Add(discovered);
                    ServerDiscovered?.Invoke(discovered);
                }

                foreach (var expired in expiredServers)
                {
                    knownServers.Add(expired);
                    ServerExpired?.Invoke(expired);
                }

                ServerListChanged?.Invoke(knownServers);
                discoveredServers.Clear();
                expiredServers.Clear();
            }
        }

        private bool searchInProgress = false;

        private readonly object knownServersLock = new object();
        private readonly HashSet<SimboxConnectionInfo> knownServers = new HashSet<SimboxConnectionInfo>();
        private Queue<SimboxConnectionInfo> discoveredServers = new Queue<SimboxConnectionInfo>();
        private Queue<SimboxConnectionInfo> expiredServers = new Queue<SimboxConnectionInfo>();

        public void StartSearch()
        {
            knownServers.Clear();
            search.BeginSearch();
            searchInProgress = true;
            ServerListChanged?.Invoke(knownServers);
        }

        public void EndSearch()
        {
            knownServers.Clear();
            search.BeginEndSearch();
            searchInProgress = false;
            ServerListChanged?.Invoke(knownServers);
        }

        private void OnServerDiscovered(SimboxConnectionInfo info)
        {
            lock (knownServersLock)
            {
                discoveredServers.Enqueue(info);
            }
        }

        private void OnServerExpired(SimboxConnectionInfo info)
        {
            lock (knownServersLock)
            {
                expiredServers.Enqueue(info);
            }
        }

        private SearchForConnections search;
    }
}