﻿// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using UnityEngine;
using UnityEngine.EventSystems;

namespace Narupa.Client.UI.Pointer
{
    /// <summary>
    /// 	Base class for which a MonoBehaviour which would like to act as a pointer to a UI canvas in VR should derive.
    /// 	It finds the VRPointerInputModule if it exists, registers itself to act as a pointer, and calls the
    /// 	UpdatePointer method to update the draw state of the pointer
    /// </summary>
    public class UiPointerSource : MonoBehaviour
    {
        void Update()
        {
            var inputModule = EventSystem.current.currentInputModule as UiPointerInputModule;
            if (inputModule == null)
                return;
            inputModule.currentPointer = transform;
            UpdatePointer(inputModule.isPointerActive, inputModule.pointerTarget);
        }

        /// <summary>
        /// 	Called to update the render state of the pointer. Use for showing/hiding the pointer and positioning the
        /// 	ray
        /// </summary>
        /// <param name="active">Whether the pointer is currently aimed at a UI canvas</param>
        /// <param name="rayTarget">The point on the Canvas where the ray has intersected</param>
        public virtual void UpdatePointer(bool active, Vector3 rayTarget)
        {
        }
    }
}