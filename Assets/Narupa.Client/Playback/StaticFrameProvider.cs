﻿// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using Narupa.Client.Network;
using NSB.Processing;

namespace Narupa.Client.Playback
{
    /// <summary>
    /// Default implementation of an IFrameProvider that serves a pregenerated frame history. After creation, call
    /// TriggerReady() to begin providing frames
    /// </summary>
    public class StaticFrameProvider : IFrameProvider
    {
        private FrameList<NSBFrame> trajectory;

        public bool IsFrameHistoryReady { get; private set; } = false;

        public event Action FrameHistoryUpdated;

        public event Action FrameHistoryReady;

        public IFrameHistory<NSBFrame> History => trajectory;
        
        public bool IsFixedHistory => true;

        public StaticFrameProvider(FrameList<NSBFrame> history)
        {
            this.trajectory = history;
        }

        public void Update()
        {
        }


        public void TriggerReady()
        {
            if (IsFrameHistoryReady) return;
            IsFrameHistoryReady = true;
            FrameHistoryReady?.Invoke();
        }
    }
}