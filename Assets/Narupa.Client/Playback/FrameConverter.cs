﻿// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Collections.Generic;
using System.Linq;
using Nano.Transport.Variables;
using Nano.Transport.Variables.Interaction;
using Narupa.Trajectory;
using NSB.Processing;
using UnityEngine;
using BondPair = Nano.Client.BondPair;
using Topology = NSB.Simbox.Topology.Topology;

namespace Narupa.Client.Playback
{
    /// <summary>
    /// Helper class for converting between frames interpreted by Narupa and frames obtained by
    /// importing molecular trajectory files
    /// </summary>
    public static class FrameConverter
    {
        /// <summary>
        /// Converts an imported trajectory to a frame list
        /// </summary>
        public static FrameList<NSBFrame> ConvertTrajectory(Narupa.Trajectory.ITrajectory traj)
        {
            var history = new FrameList<NSBFrame>();

            long i = 0;
            foreach (var frame in traj)
            {
                history.Add(ConvertFrame(i++, frame));
            }

            return history;
        }

        /// <summary>
        /// Converts an individual imported frame to a NSB frame
        /// </summary>
        /// <param name="framenumber"></param>
        /// <param name="frame"></param>
        /// <returns></returns>
        private static NSBFrame ConvertFrame(long framenumber, IFrame frame)
        {
            var topology = frame.Topology;
            var atomCount = topology.NumberOfAtoms;
            var bondCount = topology.Bonds.Count();

            return new NSBFrame()
            {
                VisibleAtomMap = Enumerable.Range(0, atomCount).ToArray(),
                FrameNumber = framenumber,
                AtomCount = atomCount,
                BondCount = bondCount,
                TopologyUpdateCount = 0,
                AtomTypes = topology.Elements.Select(e => (ushort) e).ToArray(),
                AtomPositions = frame.Positions.Select(v => new Vector3(v.X, v.Y, v.Z)).ToArray(),
                AtomVelocitiesValid = false,
                AtomVelocities = null,
                BondPairs = topology.Bonds.Select(ConvertBond).ToArray(),
                SelectedAtomIndicies = new ushort[0],
                CollidedAtomIndicies = new ushort[0],
                Topology = new Topology(),
                Variables = new Dictionary<VariableName, object>(),
                VRInteractions = new VRInteraction[0],
                InteractionForceInfo = new InteractionForceInfo[0]
            };
        }

        /// <summary>
        /// Converts from a bondpair in an imported trajectory to one used for visualisation
        /// </summary>
        private static BondPair ConvertBond(Narupa.Trajectory.BondPair bondPair)
        {
            return new BondPair()
            {
                A = (ushort) bondPair.A,
                B = (ushort) bondPair.B
            };
        }
    }
}