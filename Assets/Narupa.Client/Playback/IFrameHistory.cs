﻿// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Collections.Generic;

namespace Narupa.Client.Playback
{
    /// <summary>
    /// Represents a history of frames, basically acting as a standard C# list
    /// </summary>
    public interface IFrameHistory<TFrame> : IList<TFrame>
    {
        int FirstFrame { get; }

        int FinalFrame { get; }
    }
}