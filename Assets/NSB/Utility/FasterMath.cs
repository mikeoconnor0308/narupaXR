﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using UnityEngine;

namespace NSB.Utility
{
    public static class FasterMath
    {
        public static Color32 HSV(float hue, float sat, float val, float a = 1f)
        {
            hue *= 6;

            int hi = ((int) hue) % 6;
            float f = hue - (int) hue;

            val *= 255;
            byte v = (byte) (val);
            byte p = (byte) (val * (1 - sat));
            byte q = (byte) (val * (1 - f * sat));
            byte t = (byte) (val * (1 - (1 - f) * sat));

            Color32 color = default(Color32);
            color.a = (byte) (a * 255);

            if (hi == 0)
            {
                color.r = v;
                color.g = t;
                color.b = p;
            }
            else if (hi == 1)
            {
                color.r = q;
                color.g = v;
                color.b = p;
            }
            else if (hi == 2)
            {
                color.r = p;
                color.g = v;
                color.b = t;
            }
            else if (hi == 3)
            {
                color.r = p;
                color.g = q;
                color.b = v;
            }
            else if (hi == 4)
            {
                color.r = t;
                color.g = p;
                color.b = v;
            }
            else
            {
                color.r = v;
                color.g = p;
                color.b = q;
            }

            return color;
        }

        public static Color32 Mul(Color32 color, float f)
        {
            color.r = (byte) (color.r * f);
            color.g = (byte) (color.g * f);
            color.b = (byte) (color.b * f);
            color.a = (byte) (color.a * f);

            return color;
        }

        public static float Lerp(float a, float b, float u)
        {
            return a * (1 - u) + b * u;
        }

        public static Vector3 Uniform(float b)
        {
            Vector3 a;

            a.x = b;
            a.y = b;
            a.z = b;

            return a;
        }

        public static void Uniform(ref Vector3 a, float b)
        {
            a.x = b;
            a.y = b;
            a.z = b;
        }
    
        public static Vector3 Add(Vector3 a, Vector3 b)
        {
            a.x += b.x;
            a.y += b.y;
            a.z += b.z;

            return a;
        }

        public static Vector3 Add(Vector3 a, Vector3 b, Vector3 c, Vector3 d)
        {
            a.x += b.x + c.x + d.x;
            a.y += b.y + c.y + d.y;
            a.z += b.z + c.z + d.z;

            return a;
        }

        public static Vector3 Sub(Vector3 a, Vector3 b)
        {
            a.x -= b.x;
            a.y -= b.y;
            a.z -= b.z;

            return a;
        }

        public static Vector3 Mul(Vector3 a, float b)
        {
            a.x *= b;
            a.y *= b;
            a.z *= b;

            return a;
        }

        public static Vector3 Mul(Vector3 a, Vector3 b)
        {
            a.x *= b.x;
            a.y *= b.y;
            a.z *= b.z;

            return a;
        }

        public static Vector3 Lerp(Vector3 a, Vector3 b, float u)
        {
            a.x = a.x * (1 - u) + b.x * u;
            a.y = a.y * (1 - u) + b.y * u;
            a.z = a.z * (1 - u) + b.z * u;

            return a;
        }

        public static float Dot(Vector3 a, Vector3 b)
        {
            return a.x * b.x + a.y * b.y * a.z * b.z;
        }

        public static Vector3 Cross(Vector3 a, Vector3 b)
        {
            a.x = a.y * b.z - a.z * a.y;
            a.y = a.z * b.x - a.x * a.z;
            a.z = a.x * b.y - a.y * a.x;

            return a;
        }

        public static Vector3 TripleCross(Vector3 a, Vector3 b, Vector3 c)
        {
            // a x (b x c) = b * (a . c) - c * (a . b)
            return Sub(Mul(b, Dot(a, c)), Mul(c, Dot(a, b)));
        }
    }
}
