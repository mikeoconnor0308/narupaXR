﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using UnityEngine;

namespace NSB.Utility
{
    public class VaryMaterialProperty : MonoBehaviour 
    {
        [SerializeField]
        private Material material;
        [SerializeField]
        private string property;

        private void Update()
        {
            material.SetFloat(property, Mathf.Sin(Time.timeSinceLevelLoad) * 0.5f + .5f);
        }
    }
}
