﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using NSB.Utility;
using UnityEngine;

namespace NSB.MMD.Base
{
    public static class MMDTools
    {
        public static void BillboardBondParticle(ref ParticleSystem.Particle particle,
            Vector3 posA,
            Vector3 posB,
            Vector3 posEye)
        {
            particle.position = FasterMath.Lerp(posA, posB, 0.5f);

            Vector3 dir = FasterMath.Sub(posB, posA);
            Vector3 eye = FasterMath.Sub(posEye, particle.position);

            particle.rotation3D = Quaternion.LookRotation(dir, eye).eulerAngles;
        }
    }
}
