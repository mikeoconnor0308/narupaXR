﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using NSB.Processing;
using NSB.Utility;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace NSB.MMD.RenderingTypes
{
#if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(StyleSource))]
    public class StyleSourceDrawer : InterfaceInspectorLinkDrawer<IStyleSource> { }
#endif

    public interface IStyleSource 
    {
        NSBStyle Style { get; }
    }

    [System.Serializable]
    public class StyleSource : InterfaceInspectorLink<IStyleSource> { }
}