﻿using System.Collections.Generic;
using System.Linq;
using NSB.Examples.Player.Control;
using NSB.MMD.RenderingTypes;
using NSB.Processing;
using NSB.Simbox.Topology;
using NSB.Utility;
using UnityEngine;
using UnityEngine.Profiling;

namespace NSB.MMD.Styles
{
    public class TopologyStyle : FrameFilter, IStyleSource
    {
        public NSBStyle Style = new NSBStyle();

        NSBStyle IStyleSource.Style => Style;

        [Header("Config")]
        [Range(0, 1)]
        public float Saturation = 0.65f;
        [Range(0, 1)]
        public float Value = 1f;
        [SerializeField]
        private SelectionSource highlightSource;
        [SerializeField]
        [Range(0, 5)]
        private int depthLimit;
        [SerializeField]
        [Range(0, 1)]
        private float spacing = 0.04f;

        private List<Color32> groupColors = new List<Color32>();
        private List<AtomGroup> renderGroups = new List<AtomGroup>();

        private void DoGroup(NSBFrame frame, AtomGroup group, int depth)
        {
            if (depthLimit == depth)
            {
                renderGroups.Add(group);
            }
            else
            {
                foreach (var child in group.Children)
                {
                    DoGroup(frame, child, depth + 1);
                }
            }
        }

        public void RefreshTopology(NSBFrame frame)
        {
            frame.Topology.UpdateVisibleAtomMap(frame.VisibleAtomMap);
            var roots = frame.Topology.AtomGroups.Values.Where(g => g.Parent == null).ToList();

            renderGroups.Clear();
            foreach (var group in roots)
            {
                DoGroup(frame, group, 0);
            }

            groupColors.Resize(renderGroups.Count);

            for (int i = 0; i < renderGroups.Count; ++i)
            {
                var group = renderGroups[i];
                float hue = i / (float)groupColors.Count;

                hue = (i * spacing) % 1;

                groupColors[i] = Color.HSVToRGB(hue, Saturation, Value);

                foreach (var atom in group.Atoms)
                {
                    int id = frame.Topology.AtomToVisibleIndexMap[atom];

                    Style.AtomColors[id] = groupColors[i];
                    Style.AtomPaletteIndices[id] = (byte)i;
                }
            }

            for (int i = 0; i < frame.AtomCount; ++i)
            {
                ushort type = frame.AtomTypes[i];

                Style.AtomRadiuses[i] = ExampleMolecularStyle.ElementRadiuses[type];
            }

            Style.InitPalette();

            for (int i = 0; i < groupColors.Count; ++i)
            {
                Style.PaletteColors[i % 256] = groupColors[i];
            }

            Style.CommitPalette();
            Style.PaletteIsValid = true;
        }

        public bool refresh;

        public override void UpdateFilter(NSBFrame frame, bool topologyChanged = false)
        {
            if (frame.Topology == null) return;

            if (highlightSource.Linked)
            {
                Style.HighlightedAtoms = highlightSource.Component.Selection;
            }

            Profiler.BeginSample("TopologyStyle.UpdateFilter");

            if (topologyChanged || refresh)
            {
                refresh = false;

                Style.AtomColors.Resize(frame.AtomCount);
                Style.AtomPaletteIndices.Resize(frame.AtomCount);
                Style.AtomRadiuses.Resize(frame.AtomCount);

                if (frame.Topology.Ready)
                {
                    RefreshTopology(frame);
                }
                else
                {
                    frame.Topology.OnReady += () => refresh = true;
                }
            }

            Profiler.EndSample();
        }
    }
}
