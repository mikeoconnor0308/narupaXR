﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Collections.Generic;
using Nano.Client;
using NSB.Processing;
using NSB.Utility;
using UnityEngine;
using UnityEngine.Assertions;

namespace NSB.MMD.MeshGeneration
{
    public class MeshGeneration : MonoBehaviour
    {
        public const int VertexLimit = 65534;

        [Header("Bond Prism")]
        [Range(3, 32)]
        public int sides;
        public bool capped;
        [Header("Atom Sphere")]
        [Range(0, 4)]
        public int subdivisions;
        private Mesh mesh;

        public MeshFilter filter;

        private void OnDrawGizmosSelected()
        {
            mesh = mesh ?? new Mesh();

            //GenerateBondPrism(mesh, sides, capped: capped);
            GenerateAtomSphere(mesh, subdivisions);

            filter.sharedMesh = mesh;

            //Gizmos.color = Color.red;
            //Gizmos.DrawMesh(mesh, transform.position, transform.rotation, transform.lossyScale);
            //Gizmos.DrawWireCube(mesh.bounds.center, mesh.bounds.extents * 2);
        }

        public static void GenerateAtomSphere(Mesh mesh, 
            int level = 0,
            bool correction = true,
            bool hemisphere = false,
            float hemisphereCull = 0f)
        {
            Assert.IsTrue(level >= 0, "Level must be positive!");

            if (hemisphere)
            {
                SphereGenerator.Hemisphere(new MeshTool(mesh), 1, level, correction, limit: hemisphereCull);
            }
            else
            {
                SphereGenerator.Sphere(new MeshTool(mesh), 1, level, correction);
            }
        }

        public static void GenerateAtomQuad(Mesh mesh, float radius = .5f)
        {
            mesh.Clear();

            mesh.vertices = new Vector3[]
            {
                new Vector3(-radius, -radius, 0),
                new Vector3( radius, -radius, 0),
                new Vector3( radius,  radius, 0),
                new Vector3(-radius,  radius, 0),
            };

            mesh.uv = new Vector2[]
            {
                new Vector2(0, 0),
                new Vector2(1, 0),
                new Vector2(1, 1),
                new Vector2(0, 1),
            };

            mesh.triangles = new int[]
            {
                0, 1, 2,
                0, 2, 3,
            };
        }

        public static void GenerateBondQuad(Mesh mesh)
        {
            mesh.Clear();

            mesh.vertices = new Vector3[]
            {
                new Vector3(-.5f, 0, -.5f),
                new Vector3( .5f, 0, -.5f),
                new Vector3( .5f, 0,  .5f),
                new Vector3(-.5f, 0,  .5f),
            };

            mesh.uv = new Vector2[]
            {
                new Vector2(0, 0),
                new Vector2(1, 0),
                new Vector2(1, 1),
                new Vector2(0, 1),
            };

            mesh.triangles = new int[]
            {
                0, 1, 2,
                0, 2, 3,
            };
        }

        public static void GenerateBondPrism(Mesh mesh, 
            int sides,
            bool twist = true,
            bool capped = true,
            bool correction = true)
        {
            Assert.IsTrue(sides >= 3, "Cannot generate a prism with fewer than 3 sides!");

            string properties = (capped ? "capped, " : "")
                                + (twist ? "twisted, " : "")
                                + (correction ? "corrected" : "");
            mesh.name = $"Bond Prism ({sides} sides, {properties})";

            int vertCount = sides * 2;
            int triCount = sides * 2;
            int sideTris = sides * 2;
        
            if (capped)
            {
                vertCount += 2;
                triCount += sides * 2;
            }
        
            var tool = new MeshTool(mesh, MeshTopology.Triangles);
            tool.SetVertexCount(vertCount);
            tool.SetIndexCount(triCount * 3, lazy: true);

            float radius = 0.5f;
            float inv = Mathf.PI * 2 / sides;

            if (correction)
            {
                // try to make the average of apoth/radius = desired radius
                // * desired = (radius + apoth) / 2
                // apoth = radius * cos(pi / sides)
                // * desired = (radius + radius * cos(pi / sides)) / 2
                // * desired = radius * (1 + cos(pi / sides)) / 2
                // rearrange to determine radius from desired radius
                // * radius = desired / ((1 + cos(pi / sides)) / 2)
            
                radius = radius * 2 / (1 + Mathf.Cos(Mathf.PI / sides));
            }

            for (int i = 0; i < sides; ++i)
            {
                float angle1 = inv * i;
                float angle2 = twist ? inv * (i + 0.5f) : angle1;

                tool.positions[i * 2 + 0] = new Vector3(Mathf.Cos(angle1) * radius,
                    Mathf.Sin(angle1) * radius,
                    -0.5f);

                tool.positions[i * 2 + 1] = new Vector3(Mathf.Cos(angle2) * radius,
                    Mathf.Sin(angle2) * radius,
                    0.5f);

                tool.uv0s[i * 2 + 0] = Vector2.zero;
                tool.uv0s[i * 2 + 1] = Vector2.one;
            }

            tool.indices[0] = 0;
            tool.indices[1] = sides * 2 - 1;
            tool.indices[2] = sides * 2 - 2;

            tool.indices[3] = sides * 2 - 1;
            tool.indices[4] = 0;
            tool.indices[5] = 1;

            for (int i = 3; i < sideTris; i += 2)
            {
                tool.indices[i * 3 + 0] = i - 2;
                tool.indices[i * 3 + 1] = i - 1;
                tool.indices[i * 3 + 2] = i - 0;
            }

            for (int i = 2; i < sideTris; i += 2)
            {
                tool.indices[i * 3 + 0] = i - 0;
                tool.indices[i * 3 + 1] = i - 1;
                tool.indices[i * 3 + 2] = i - 2;
            }

            if (capped)
            {
                int bot = vertCount - 2;
                int top = vertCount - 1;

                float cap = .5f + .2f;

                tool.positions[bot] = new Vector3(0, 0, -cap);
                tool.positions[top] = new Vector3(0, 0,  cap);

                tool.uv0s[bot] = Vector3.zero;
                tool.uv0s[top] = Vector3.one;

                int offset = sideTris * 3;

                for (int i = 1; i < sides; ++i)
                {
                    tool.indices[offset + 0] = bot;
                    tool.indices[offset + 2] = (i - 1) * 2;
                    tool.indices[offset + 1] = (i - 0) * 2;

                    offset += 3;
                }

                tool.indices[offset + 0] = bot;
                tool.indices[offset + 2] = (sides - 1) * 2;
                tool.indices[offset + 1] = 0;

                offset += 3;

                for (int i = 1; i < sides; ++i)
                {
                    tool.indices[offset + 0] = top;
                    tool.indices[offset + 1] = (i - 1) * 2 + 1;
                    tool.indices[offset + 2] = (i - 0) * 2 + 1;

                    offset += 3;
                }

                tool.indices[offset + 0] = top;
                tool.indices[offset + 1] = (sides - 1) * 2 + 1;
                tool.indices[offset + 2] = 1;
            }
        
            tool.Apply(positions: true, uv0s: true, indices: true, autoNormals: true);
        }

        [ThreadStatic]
        private static List<int> id2index;
        [ThreadStatic]
        private static List<BondPair> filteredBonds;
        public static void GenerateBondLines(MeshTool mesh, 
            NSBFrame frame,
            NSBSelection selection,
            NSBStyle style)
        {
            filteredBonds = filteredBonds ?? new List<BondPair>();
            selection.FilterBonds(frame, filteredBonds);

            id2index = id2index ?? new List<int>();
            id2index.Resize(frame.AtomCount);

            mesh.SetVertexCount(selection.Count);
            mesh.SetIndexCount(filteredBonds.Count * 2, lazy: true);

            // vertex per selected atom
            for (int i = 0; i < selection.Count; ++i)
            {
                int id = selection[i];
                id2index[id] = i;

                mesh.positions[i] = frame.AtomPositions[id];
                mesh.colors[i] = style.AtomColors[id];
            }

            // line between each pair of bonded vertices
            for (int i = 0; i < filteredBonds.Count; ++i)
            {
                var bond = filteredBonds[i];
            
                mesh.indices[i * 2 + 0] = id2index[bond.A];
                mesh.indices[i * 2 + 1] = id2index[bond.B];
            }
        
            mesh.mesh.Clear();
            mesh.Apply(positions: true, 
                colors: true, 
                indices: true);
        }

        public static void GenerateAtomPoints(MeshTool mesh,
            NSBFrame frame,
            NSBSelection selection,
            NSBStyle style)
        {
            mesh.mesh.Clear();

            if (selection.Count == 0) return;

            mesh.SetVertexCount(selection.Count);
            mesh.SetIndexCount(selection.Count, lazy: true);

            // vertex per selected atom
            for (int i = 0; i < selection.Count; ++i)
            {
                int id = selection[i];

                mesh.positions[i] = frame.AtomPositions[id];
                mesh.colors[i] = style.AtomColors[id];
                mesh.indices[i] = i;
            }

            mesh.Apply(positions: true,
                colors: true,
                indices: true);
        }
    }
}
