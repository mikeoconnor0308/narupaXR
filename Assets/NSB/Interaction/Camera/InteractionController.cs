﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using UnityEngine;

namespace NSB.Interaction.Camera
{
    enum InteractionMode
    {
        None,
        Pan,
        Field,
    }

    class InteractionController : MonoBehaviour
    {
        [HideInInspector]
        public IInteraction ActiveInteraction;

        [SerializeField]
        public IInteraction[] Interactions;

        // Use this for initialization
        void Start()
        {
            foreach (IInteraction interaction in Interactions)
            {
                interaction.Enabled = true;
            }
        }

        // Update is called once per frame
        void Update()
        {

            if (ActiveInteraction != null)
            {
                if (ActiveInteraction.UpdateInteractions() == false)
                {
                    ActiveInteraction = null;
                }
            }

            if (ActiveInteraction == null)
            {
                foreach (IInteraction interaction in Interactions)
                {
                    if (interaction.Enabled == false)
                    {
                        continue;
                    }

                    if (interaction.UpdateInteractions())
                    {
                        ActiveInteraction = interaction;
                        break;
                    }
                }
            }
        }
    }
}