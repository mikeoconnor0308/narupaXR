﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Collections.Generic;
using Nano.Client;
using NSB.Examples.Player.Control;
using NSB.Utility;
using UnityEngine;

namespace NSB.Examples.Player.Screens.Server_Select
{
    public class ServerSelectionPanel : MonoBehaviour
    {
        [SerializeField]
        private ExampleAppController app;
        [SerializeField]
        private InstancePoolSetup serversSetup;
        private InstancePool<SimboxConnectionInfo> servers;

        private SearchForConnections search;
        private HashSet<SimboxConnectionInfo> knownServers = new HashSet<SimboxConnectionInfo>();
        private object knownServersLock = new object();

        public void SelectServer(SimboxConnectionInfo info)
        {
            app.EnterServer(info);
        }

        private void Awake()
        {
            servers = serversSetup.Finalise<SimboxConnectionInfo>(sort: false);

            search = new SearchForConnections();
            search.DeviceDiscovered += OnServerDiscovered;
            search.DeviceExpired += OnServerExpired;
        }

        private void Update()
        {
            lock (knownServersLock)
            {
                servers.SetActive(knownServers);
            }
        }

        private void OnServerDiscovered(SimboxConnectionInfo info)
        {
            lock (knownServersLock)
            {
                knownServers.Add(info);
            }
        }

        private void OnServerExpired(SimboxConnectionInfo info)
        {
            lock (knownServersLock)
            {
                knownServers.Remove(info);
            }
        }

        private void OnEnable()
        {
            lock (knownServersLock)
            {
                knownServers.Clear();
            }

            search.BeginSearch();
        }

        private void OnDisable()
        {
            search.BeginEndSearch();
        }
    }
}
