﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using NSB.Examples.Player.Control;
using UnityEngine;
using UnityEngine.UI;

namespace NSB.Examples.Player.Screens
{
    public class SimulationHUDPanel : MonoBehaviour
    {
        [SerializeField]
        private PlaybackRenderer playback;
        [SerializeField]
        private Slider timeSlider;
        [SerializeField]
        private GameObject mediaControls;

        private void Awake()
        {
            timeSlider.onValueChanged.AddListener(frame => playback.SetFrame(frame));
        }

        private void Update()
        {
            /*
            if (!playback.Reader.Ready)
            {
                return;
            }

            mediaControls.SetActive(playback.Reader.Networked);

            timeSlider.minValue = playback.Reader.history.FirstFrame;
            timeSlider.maxValue = playback.Reader.history.FinalFrame;
            timeSlider.value = playback.CurrentFrame;
            */
        }
    }
}
