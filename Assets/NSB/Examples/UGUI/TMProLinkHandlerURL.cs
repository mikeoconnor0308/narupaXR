﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using NSB.Examples.Utility;
using UnityEngine;

namespace NSB.Examples.UGUI
{
    public class TMProLinkHandlerURL : TMProLinkHandler
    {
        private void Start()
        {
            OnLinkClicked += (link, text) =>
            {
                Application.OpenURL(link);
            };
        }
    }
}
