﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using UnityEngine;
using UnityEngine.UI;

namespace NSB.Examples.UGUI
{
    public class OnToggledOn : MonoBehaviour
    {
        [System.Serializable]
        public class Action : UnityEngine.Events.UnityEvent { };

        [SerializeField]
        private Toggle toggle;

        public Action onToggledOn;
        public Action onToggledOff;

        private void Awake()
        {
            toggle.onValueChanged.AddListener(active =>
            {
                if (active)
                {
                    onToggledOn.Invoke();
                }
                else
                {
                    onToggledOff.Invoke();
                }
            });
        }
    }
}
