﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace NSB.Examples.UGUI
{
    public class UIClicks : MonoBehaviour, IPointerClickHandler
    {
        [Serializable]
        public class OnAction : UnityEvent { };

        public OnAction onSingleClick = new OnAction();
        public OnAction onDoubleClick = new OnAction();

        void IPointerClickHandler.OnPointerClick(PointerEventData eventData)
        {
            if (eventData.clickCount == 1)
            {
                onSingleClick.Invoke();
            }
            else if (eventData.clickCount == 2)
            {
                onDoubleClick.Invoke();
            }
        }
    }
}
