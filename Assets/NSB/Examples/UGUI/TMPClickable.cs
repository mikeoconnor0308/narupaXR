﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using NSB.Examples.Utility;
using UnityEngine;
using UnityEngine.EventSystems;
using Text = TMPro.TextMeshProUGUI;

namespace NSB.Examples.UGUI
{
    public class TMPClickable : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField]
        private Text text;
        [SerializeField]
        private TMProLinkHandler handler;

        void IPointerClickHandler.OnPointerClick(PointerEventData eventData)
        {
            int index = TMPro.TMP_TextUtilities.FindIntersectingLink(text, eventData.position, null);

            if (index != -1)
            {
                var link = text.textInfo.linkInfo[index];

                handler.HandleLink(link.GetLinkID(), link.GetLinkText());
            }
        }
    }
}
