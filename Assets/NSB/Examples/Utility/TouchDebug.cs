﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using UnityEngine;
using Text = TMPro.TextMeshProUGUI;

namespace NSB.Examples.Utility
{
    public class TouchDebug : MonoBehaviour 
    {
        [SerializeField]
        private Text text;

        private void Update()
        {
            string text = "";

            for (int i = 0; i < Input.touchCount; ++i)
            {
                var touch = Input.GetTouch(i);

                text += string.Format("{0} ({1}) {2} at {3}\n", i, touch.fingerId, touch.phase, touch.position);
            }

            this.text.text = text;
        }
    }
}
