﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using NSB.Utility;
using UnityEngine;

namespace NSB.Examples.Utility
{
    public struct Sphere
    {
        public Vector3 position;
        public float radius;

        public Sphere(Vector3 position, float radius)
        {
            this.position = position;
            this.radius = radius;
        }

        public bool Raycast(Ray ray, out float t)
        {
            var m = FasterMath.Sub(ray.origin, position);
            var b = Vector3.Dot(m, ray.direction);
            var c = Vector3.Dot(m, m) - radius * radius;

            if (c > 0 && b > 0)
            {
                t = 0;
                return false;
            }

            float discr = b * b - c;

            if (discr < 0)
            {
                t = 0;
                return false;
            }

            t = Mathf.Max(0, -b - Mathf.Sqrt(discr));

            return true;
        }
    }
}
