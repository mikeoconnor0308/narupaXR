﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using UnityEngine;
using UnityEngine.EventSystems;
using Action = System.Action;

namespace NSB.Examples.Utility
{
    public class DragControl : MonoBehaviour,
        IPointerDownHandler,
        IPointerUpHandler,
        IDragHandler
    {
        [SerializeField]
        private CanvasGroup group;

        public Action OnBegin = delegate { };
        public Action OnEnd = delegate { };

        public Vector2 Displacement { get; private set; }
        public bool Dragging { get; private set; }

        private Vector2 origin;

        void IPointerDownHandler.OnPointerDown(PointerEventData eventData)
        {
            Dragging = true;
            Displacement = Vector2.zero;
            origin = eventData.position;

            group.ignoreParentGroups = true;

            OnBegin();
        }

        void IPointerUpHandler.OnPointerUp(PointerEventData eventData)
        {
            Dragging = false;
            Displacement = Vector2.zero;

            group.ignoreParentGroups = false;

            OnEnd();
        }

        void IDragHandler.OnDrag(PointerEventData eventData)
        {
            Displacement = eventData.position - origin;
        }
    }
}
