﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using UnityEngine;
using UnityEngine.EventSystems;

namespace NSB.Examples.Utility
{
    public class Draggable : MonoBehaviour, IDragHandler
    {
        void IDragHandler.OnDrag(PointerEventData eventData)
        {
            var rtrans = transform as RectTransform;

            rtrans.position += (Vector3) eventData.delta;
        }
    }
}
