﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using UnityEngine;
using UnityEngine.Events;

namespace NSB.Examples.Utility
{
    public class FiveTouchTrigger : MonoBehaviour
    {
        [System.Serializable]
        public class OnPressed : UnityEvent { }

        public OnPressed onPressed;

        private void Update()
        {
            if (Input.touchCount >= 5)
            {
                onPressed.Invoke();
            }
        }
    }
}