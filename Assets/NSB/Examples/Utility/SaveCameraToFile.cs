﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using UnityEngine;

namespace NSB.Examples.Utility
{
    public class SaveCameraToFile : MonoBehaviour
    {
        [SerializeField]
        private new Camera camera;

        [ContextMenu("Save Texture")]
        public void Save()
        {
            var render = new RenderTexture(camera.pixelWidth, camera.pixelHeight, 32);
            var target = new Texture2D(camera.pixelWidth, camera.pixelHeight, TextureFormat.ARGB32, false);

            camera.targetTexture = render;
            camera.Render();

            RenderTexture.active = render;
            target.ReadPixels(new Rect(0, 0, target.width, target.height), 0, 0);
            RenderTexture.active = null;

            camera.targetTexture = null;

            Destroy(render);

            System.IO.File.WriteAllBytes("screenshot.png", target.EncodeToPNG());

            Destroy(target);
        }
    }
}
