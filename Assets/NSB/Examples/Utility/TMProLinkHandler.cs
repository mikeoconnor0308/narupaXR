﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using UnityEngine;

namespace NSB.Examples.Utility
{
    public class TMProLinkHandler : MonoBehaviour
    {
        public System.Action<string, string> OnLinkClicked = delegate
            { };

        public void HandleLink(string link, string text)
        {
            OnLinkClicked(link, text);
        }
    }
}
