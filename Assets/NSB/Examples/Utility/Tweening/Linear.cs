﻿namespace NSB.Examples.Utility.Tweening
{
    class Linear : IEaser
    {
        public float EaseIn(float ratio)
        {
            return ratio;
        }

        public float EaseOut(float ratio)
        {
            return ratio;
        }

        public float EaseInOut(float ratio)
        {
            return ratio;
        }
    }
}
