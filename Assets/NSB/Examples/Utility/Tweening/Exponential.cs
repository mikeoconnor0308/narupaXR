﻿using System;

namespace NSB.Examples.Utility.Tweening
{
    class Exponential : IEaser
    {
        public float EaseIn(float ratio)
        {
            return (ratio == 0f) ? 0f : (float)Math.Pow(2f, 10f * (ratio - 1));
        }

        public float EaseOut(float ratio)
        {
            return (ratio == 1f) ? 1f : 1f - (float)Math.Pow(2f, -10f * ratio);
        }

        public float EaseInOut(float ratio)
        {
            if (ratio == 0f || ratio == 1f) { return ratio; }
            if (0 > (ratio = ratio * 2 - 1)) { return 0.5f * (float)Math.Pow(2f, 10f * ratio); }
            return 1f - 0.5f * (float)Math.Pow(2f, -10f * ratio);
        }
    }
}
