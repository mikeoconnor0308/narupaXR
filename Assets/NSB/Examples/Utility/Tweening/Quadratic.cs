﻿namespace NSB.Examples.Utility.Tweening
{
    class Quadratic : IEaser
    {
        public float EaseIn(float ratio)
        {
            return ratio * ratio;
        }

        public float EaseOut(float ratio)
        {
            return -ratio * (ratio - 2f);
        }

        public float EaseInOut(float ratio)
        {
            return (ratio < 0.5f) ? 2f * ratio * ratio : -2f * ratio * (ratio - 2f) - 1f;
        }
    }
}
