﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using Nano;
using Nano.Transport.Agents;
using Nano.Transport.Streams;
using UnityEngine;
using ClientBase = Nano.Client.ClientBase;

namespace NSB.Simbox
{
    public class SimboxSyncBuffer<TData> : ISimboxSyncBuffer<TData>
    { 
        public readonly ushort StreamID;
        public readonly object Lock = new object();

        /// <summary>
        /// Reset to true when the buffer has been updated - to be set to false
        /// by API user
        /// </summary>
        public bool IsDirty { get; set; }

        /// <summary>
        /// Gets the data stream instance.
        /// </summary>
        public IDataStream<TData> Stream { get; private set; }

        /// <summary>
        ///
        /// </summary>
        public TData[] Data { get; private set; }

        /// <summary>
        ///
        /// </summary>
        public int Count { get; private set; }

        public event SimboxSyncBufferUpdate<TData> Updated; 

        public SimboxSyncBuffer(ushort id)
        {
            StreamID = id;
        }

        public void Attach(ClientBase client)
        {
            client.IncomingStreams.Added += OnInputsAdded;
            client.IncomingStreams.Removed += OnInputsRemoved;
        }

        public void Detach(ClientBase client)
        {
            client.IncomingStreams.Added -= OnInputsAdded;
            client.IncomingStreams.Removed -= OnInputsRemoved;

            Count = 0;
            IsDirty = true;
        }

        private void OnInputsAdded(IncomingDataAgent agent, ushort streamID, TransportDataStreamReceiver dataStreamReceiver)
        {
            if (streamID == StreamID)
            {
                dataStreamReceiver.StreamCreated += OnStreamCreated;
                dataStreamReceiver.StreamDisposed += OnStreamDisposed;
            }
        }

        private void OnInputsRemoved(IncomingDataAgent agent, ushort streamID, TransportDataStreamReceiver dataStreamReceiver)
        {
            if (streamID == StreamID)
            {
                dataStreamReceiver.StreamCreated -= OnStreamCreated;
                dataStreamReceiver.StreamDisposed -= OnStreamDisposed;
            }
        }

        private void OnStreamCreated(TransportDataStreamReceiver receiver, ITransportDataStream stream)
        {
            lock (Lock)
            {
                Stream = stream as IDataStream<TData>;
                Data = new TData[stream.MaxCount];
            }

            receiver.Updated += OnUpdated;
        }

        private void OnStreamDisposed(TransportDataStreamReceiver receiver, ITransportDataStream stream)
        {
            receiver.Updated -= OnUpdated;

            Stream = null;
        }

        private void OnUpdated(object sender, EventArgs e)
        {
            if (Stream == null)
            {
                return;
            }

            Count = Stream.Count;

            Array.ConstrainedCopy(Stream.Data, 0, Data, 0, Stream.Count);

            IsDirty = true;

            if (Updated != null)
            {
                Updated(this); 
            } 
        }
    }

    public class SimboxSyncBuffer_UncompressHalf3ToVector3 : ISimboxSyncBuffer<Vector3>
    {
        /// <summary>
        /// ID of the stream.
        /// </summary>
        public readonly ushort ID;

        /// <summary>
        /// Sync lock object.
        /// </summary>
        public readonly object Lock = new object();

        /// <summary>
        /// Gets a flag indicating that the buffer has been updated and needs to be copied.
        /// </summary>
        public bool IsDirty { get; set; }

        /// <summary>
        ///
        /// </summary>
        public Vector3[] Data { get; private set; }

        /// <summary>
        ///
        /// </summary>
        public int Count { get; private set; }

        private bool isStreamCompressed;

        private IDataStream<Vector3> uncompressedDataStream;
        private IDataStream<SlimMath.Half3> compressedDataStream;

        public event SimboxSyncBufferUpdate<Vector3> Updated;

        public SimboxSyncBuffer_UncompressHalf3ToVector3(ushort id)
        {
            ID = id;
        }

        public void Attach(ClientBase client)
        {
            client.IncomingStreams.Added += Inputs_Added;
            client.IncomingStreams.Removed += Inputs_Removed;
        }

        public void Detach(ClientBase client)
        {
            client.IncomingStreams.Added -= Inputs_Added;
            client.IncomingStreams.Removed -= Inputs_Removed;

            Count = 0;
            IsDirty = true;
        }

        private void Inputs_Added(IncomingDataAgent agent, ushort streamID, TransportDataStreamReceiver dataStreamReceiver)
        {
            if (streamID == ID)
            {
                dataStreamReceiver.StreamCreated += OnStreamCreated;
                dataStreamReceiver.StreamDisposed += OnStreamDisposed;
            }
        }

        private void Inputs_Removed(IncomingDataAgent agent, ushort streamID, TransportDataStreamReceiver dataStreamReceiver)
        {
            if (streamID == ID)
            {
                dataStreamReceiver.StreamCreated -= OnStreamCreated;
                dataStreamReceiver.StreamDisposed -= OnStreamDisposed;
            }
        }

        private void OnStreamCreated(TransportDataStreamReceiver receiver, ITransportDataStream stream)
        {
            lock (Lock)
            {
                if (stream is IDataStream<Vector3>)
                {
                    uncompressedDataStream = stream as IDataStream<Vector3>;
                    isStreamCompressed = false;
                }
                else if (stream is IDataStream<SlimMath.Half3>)
                {
                    compressedDataStream = stream as IDataStream<SlimMath.Half3>;
                    isStreamCompressed = true;
                }
                else
                {
                    throw new SimboxException(SimboxExceptionType.TypesAreIncompatible, "Stream " + stream.Format.ToString() + " is not a Vector3 or Half3 stream.");
                }

                Data = new Vector3[stream.MaxCount];
            }

            receiver.Updated += OnUpdated;
        }

        private void OnStreamDisposed(TransportDataStreamReceiver receiver, ITransportDataStream stream)
        {
            receiver.Updated -= OnUpdated;

            uncompressedDataStream = null;
            compressedDataStream = null;
        }

        private void OnUpdated(object sender, EventArgs e)
        {
            if (isStreamCompressed == false)
            {
                if (uncompressedDataStream == null)
                {
                    return;
                }

                Count = uncompressedDataStream.Count;

                Array.ConstrainedCopy(uncompressedDataStream.Data, 0, Data, 0, uncompressedDataStream.Count);
            }
            else
            {
                if (compressedDataStream == null)
                {
                    return;
                }

                Count = compressedDataStream.Count;

                SlimMath.Half3[] source = compressedDataStream.Data;
                Vector3[] destination = Data;

                for (int i = 0, ie = Math.Min(Count, source.Length); i < ie; i++)
                {
                    SlimMath.Vector3 v3 = source[i];

                    destination[i] = new Vector3(v3.X, v3.Y, v3.Z);
                }
            }

            IsDirty = true;

            if (Updated != null)
            {
                Updated(this);
            }
        }
    }
}
