﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Nano;
using Nano.Client;
using Nano.Transport.Agents;

namespace NSB.Simbox
{
    class SimboxConnection : SimboxConnectionBase
    {
        public SimboxConnection(SimboxConnectionInfo info, IReporter reporter, string recordingPath = null) 
            : base(info, reporter, recordingPath)
        {

        }

        protected override ClientBase CreateClient(PacketStatistics statistics)
        {
            return new Client(ConnectionImplementation.Transmitter, statistics, Reporter);
        }
    }
}
