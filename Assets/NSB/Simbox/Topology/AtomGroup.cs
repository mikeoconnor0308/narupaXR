﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Collections.Generic;

namespace NSB.Simbox.Topology
{
    /// <summary>
    ///     Represents a group of atoms that share a common address.
    /// </summary>
    public class AtomGroup
    {
        /// <summary>
        ///     The OSC Address of the group.
        /// </summary>
        public string Address;

        /// <summary>
        ///     The atoms present in this atom group.
        /// </summary>
        public HashSet<TopologyAtom> Atoms = new HashSet<TopologyAtom>();

        /// <summary>
        ///     The list of child atom groups.
        /// </summary>
        public HashSet<AtomGroup> Children = new HashSet<AtomGroup>();

        /// <summary>
        ///     The id of this atom group.
        /// </summary>
        /// <remarks>
        ///     Is not always required, but is useful for representing Residue IDs in proteins.
        /// </remarks>
        public int Id;

        /// <summary>
        ///     The name of this atom group.
        /// </summary>
        public string Name;

        /// <summary>
        ///     The parent of this atom group.
        /// </summary>
        public AtomGroup Parent;

        public AtomGroup(string address, string name, int id = 0, AtomGroup parent = null)
        {
            Id = id;
            Address = address;
            Name = name;
            Parent = parent;
        }
    }
}